#include <iostream>
#include <string>
#include <unordered_map>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "flowshop-solver/MHSolve.h"

namespace py = pybind11;

PYBIND11_MODULE(pyflowshop, m) {
    m.doc() = "flowshop solver with metaheuristics from paradiseo"; // optional module docstring
    m.def("init", &initFactories, "Initialize instances and specs");
    m.def("solve", &solveWithFactories, "Solve the flowshop problem");
}
