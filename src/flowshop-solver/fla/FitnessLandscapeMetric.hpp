#pragma once

class FitnessLandscapeMetric {
 public:
  virtual double compute() = 0;
};
