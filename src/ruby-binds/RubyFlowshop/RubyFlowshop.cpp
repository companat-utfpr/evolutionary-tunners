#include "rice/Class.hpp"
#include "rice/Hash.hpp"
#include "rice/String.hpp"

#include "fsp/FSPOrderHeuristics.h"
#include "meta/MHSolve.h"
#include "meta/fspproblemfactory.h"
#include "meta/specsdata.h"
#include "utils/RNG.h"

using namespace Rice;

/**
 * Initialize factories
 */
Object init(Object /* self */, String specs_folder, String instances_folder) {
  MHParamsSpecsFactory::init(specs_folder.str());
  FSPProblemFactory::init(instances_folder.str());
  return true;
}

/**
 * Solve a FSP problem with an metaheuristic and params, returns the best result
 */

int no_solves;
Object solve(Object /* self */, String mh_name, Hash problem, long seed,
             Hash params) {
  std::unordered_map<std::string, std::string> prob_data;
  std::unordered_map<std::string, float> params_data;
  // std::cout << "Problem\n";
  for (auto kv : problem) {
    auto k = String(kv.first).str();
    auto v = String(kv.second).str();
    prob_data[k] = v;
    // std::cout << k << ':' << v << '\n';
  }
  // std::cout << "Params\n";
  for (auto kv : params) {
    auto k = String(kv.first).str();
    auto v_str = String(kv.second).str();
    params_data[k] = std::atof(v_str.c_str());
    // std::cout << k << ':' << v_str << '\n';
  }
  double result =
      solveWithFactories(mh_name.str(), prob_data, seed, params_data);
  std::cerr << "no_solves: " << ++no_solves << '\n';
  return to_ruby(result);
}

void handle_error(const std::runtime_error &re) {
  throw Exception(rb_eRuntimeError, re.what());
}

extern "C" void Init_rubyflowshop() {
  RUBY_TRY {
    Class rb_cRubyFlowshop = define_class("RubyFlowshop")
                                 .add_handler<std::runtime_error>(handle_error)
                                 .define_method("init", &init)
                                 .define_method("solve", &solve);
  }
  RUBY_CATCH
}
