# Load in the extension (on OS X this loads ./MyTest/mytest.bundle - unsure about Linux, possibly mytest.so)
require 'rubyflowshop'

# MyTest is now a module, so we need to include it


project_root = '/home/lucasmp/projects/git/evolutionary_tunners/'
specs = project_root + "data/specs"
instances = project_root + "data/instances/generated_intances/generated_instances_taill-like/"

rfsp = RubyFlowshop.new

rfsp.init(specs, instances)

10.times do |i|
  # Call and print the result from the test1 method
  result = rfsp.solve("HC", "taill-like_20_10_2010101.gen:PERM", i, {
      "HC.Algo"             => 2.0,
      "Comp.Strat"          => 1.0,
      "Init.Strat"          => 2.0,
      "Neighborhood.Size"   => 5.5,
      "Neighborhood.Strat"  => 0.0
    })
  puts result
end
# => 10
