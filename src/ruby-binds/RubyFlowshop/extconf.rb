# Loads mkmf which is used to make makefiles for Ruby extensions
require 'mkmf-rice'

# Give it a name
extension_name = 'rubyflowshop'

$CXXFLAGS += " -std=c++14 -shared "
$LIBS += " -lpthread -lstdc++fs -lflowshop_solver_lib -lutils_lib -leo -leoutils "
# The destination
#dir_config(extension_name)

project_root = '/home/lucasmp/projects/git/evolutionary_tunners/'

dir_config('paradiseo',
           project_root + 'build/paradiseo/include/paradiseo/eo',
           project_root + 'build/paradiseo/lib64/')
dir_config('',
          project_root + 'build/paradiseo/include/paradiseo/mo',
          project_root + 'build/paradiseo/lib64/')
dir_config('',
           project_root + 'build/paradiseo/include/paradiseo/moeo',
           project_root + 'build/paradiseo/lib64/')
dir_config('utils', project_root + 'src/',
                    project_root + 'build/src/utils')
dir_config('flowshop_solver', project_root + 'src/flowshop-solver/',
                       project_root + 'build/src/flowshop-solver/')
#dir_config('flowshop-solver', '<Path to include dir>', '<Path to lib dir>')

# Do the work
create_makefile(extension_name)
