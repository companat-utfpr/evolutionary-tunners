# Evolutionary Solvers

## Pastas

- data: arquivos de dados (não código), instâncias, parâmetros de entrada, etc
- doc: documentação
- main: arquivos com executáveis (um para cada executável)
- src: código das bibliotecas (não executáveis)
- test: códigos de testes
- bench: códigos de benchmark
- scripts: scripts para testes e execuções
- lib (não comitada): bibliotecas externas
- runs (não comitada): execuções
- build (não comitada): arquivos binários

## Códigos

- src/flowshop-solver: código para avaliação do fsp e metaheurísticas
- src/hppso: HP-PSO original da prof. Myriam
- src/pso-tuner: versão nova do HP-PSO (ICs)
