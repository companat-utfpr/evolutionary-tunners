library(keras)
library(caret)
library(broom)
source("R/models/model_utils.R")



getRecModelPredictions <- function(x_train, y_train, x_test) {
  no_inputs <- ncol(x_train)
  no_outputs <- ncol(y_train)
  model <- keras_model_sequential() 
  model %>% 
    layer_batch_normalization(input_shape = ncol(x_train)) %>%
    layer_dense(units = 128, activation = 'relu', input_shape = no_inputs, kernel_initializer = 'glorot_normal') %>% 
    layer_dropout(rate = 0.4) %>%
    layer_dense(units = 64, activation = 'relu', kernel_initializer = 'glorot_normal') %>%
    layer_dropout(rate = 0.3) %>%
    layer_dense(units = no_outputs, activation = 'sigmoid', kernel_initializer = 'glorot_normal')
  summary(model)
  
  model %>% compile(
    loss = "binary_crossentropy",
    optimizer = optimizer_rmsprop(),
    metrics = rep('binary_accuracy', 1)
  )
  
  ties <- y_train %>% apply(1, sum) == 0
  sample_weight <- rep(1, nrow(y_train))
  sample_weight[ties] <- 0.5
  history <- model %>% fit(
    x_train, y_train,
    epochs = 300, batch_size = 128, 
    validation_split = 0.0,
    sample_weight = sample_weight
    # ,callbacks = list(callback_early_stopping(monitor = 'val_loss'))#,
    # ,class_weight = list(`0` = 2, `1` = 1)
  )
  
  prediction <- round(clampMatrix(predict(model, x_test), 0, 1))
  colnames(prediction) <- colnames(y_train)
  prediction
}



getRankModelPredictions <- function(x_train, y_train, x_test) {
  no_inputs <- ncol(x_train)
  no_outputs <- ncol(y_train)
  model <- keras_model_sequential() 
  model %>% 
    layer_batch_normalization(input_shape = ncol(x_train)) %>%
    layer_dense(units = 128, activation = 'relu', input_shape = no_inputs, kernel_initializer = 'glorot_normal') %>% 
    layer_dropout(rate = 0.4) %>%
    layer_dense(units = 64, activation = 'relu', kernel_initializer = 'glorot_normal') %>%
    layer_dropout(rate = 0.3) %>%
    layer_dense(units = no_outputs, activation = 'softsign', kernel_initializer = 'glorot_normal')
  summary(model)
  
  
  k <- backend()
  accuracy2 <- custom_metric(
    "accuracy2", function(y_true, y_pred) {
      metric_binary_accuracy(k$round(y_true+1), k$round(y_pred + 1))
    })
  
  model %>% compile(
    loss = "mse",
    optimizer = optimizer_rmsprop(),
    metric = accuracy2
  )
  
  ties <- y_train %>% apply(1, sum) == 0
  sample_weight <- rep(1, nrow(y_train))
  sample_weight[ties] <- 0.5
  history <- model %>% fit(
    x_train, y_train,
    epochs = 300, batch_size = 128, 
    validation_split = 0.0,
    sample_weight = sample_weight
    # ,callbacks = list(callback_early_stopping(monitor = 'val_loss'))#,
    # ,class_weight = list(`0` = 2, `1` = 1)
  )
  
  prediction <- round(predict(model, x_test) + 1)
  colnames(prediction) <- colnames(y_train)
  prediction
}


set.seed(123456)
use_session_with_seed(123456)

bin_rec_model_data <- loadModelData("MH recommendation")
rec_input_matrix <- inputMatrix(bin_rec_model_data) %>% 
  normalizeMatrix(-1, 1)
rec_output_matrix <- outputMatrix(bin_rec_model_data, "MH recommendation") %>%
  normalizeMatrix(0, 1)

rank_model_data <- loadModelData("MH pairwise ranking")
rank_input_matrix <- inputMatrix(rank_model_data) %>% 
  normalizeMatrix(-1, 1)
rank_output_matrix <- outputMatrix(rank_model_data, "MH pairwise ranking")

folds <- createFolds(runif(nrow(rec_input_matrix)), k = 5)

test_results <- NULL

for (fold in folds) {
  rec_predictions <- getRecModelPredictions(
    rec_input_matrix[-fold,],
    rec_output_matrix[-fold,], 
    rec_input_matrix[fold,]
  )
  rank_predictions <- getRankModelPredictions(
    rank_input_matrix[-fold,],
    rank_output_matrix[-fold,], 
    rank_input_matrix[fold,]
  )
  
  y_test <- rec_output_matrix[fold,] %>% as.matrix()
  colnames(y_test) <- paste("true", colnames(y_test))
  fold_test_results <- bind_cols(
    y_test %>% as_tibble(),
    rec_predictions %>% as_tibble(), 
    rank_predictions %>% as_tibble()
  )
  test_results <- bind_rows(test_results, fold_test_results)
}

results_with_ranks <- test_results %>%
  mutate(
    true_recs = pmap(., function(...) {
      scores <- c()
      pcomps <- list(...)
      print(pcomps)
      pcomps <- pcomps[str_detect(names(pcomps), "true .*")]
      print(pcomps)
      for (comp in names(pcomps)) {
        mhs <- str_split(comp, "_")[[1]]
        if (pcomps[comp] == 1) {
          scores <- c(scores, toupper(mhs[2]))
        }
      }
      print(scores)
      scores
    }),
    pred_recs = pmap(., function(...) {
      scores <- c()
      pcomps <- list(...)
      print(pcomps)
      pcomps <- pcomps[str_detect(names(pcomps), "^rec_.*")]
      print(pcomps)
      for (comp in names(pcomps)) {
        mhs <- str_split(comp, "_")[[1]]
        if (pcomps[comp] == 1) {
          scores <- c(scores, toupper(mhs[2]))
        }
      }
      print(scores)
      scores
    }),
    ranks = pmap(., function(...) {
      scores <- rep(0, length(ALL_MHS))
      names(scores) <- ALL_MHS
      pcomps <- list(...)
      print(pcomps)
      pcomps <- pcomps[str_detect(names(pcomps), ".* x .*")]
      print(pcomps)
      for (comp in names(pcomps)) {
        mhs <- str_split(comp, " x ")[[1]]
        if (pcomps[comp] == 0) {
          scores[mhs[2]] <- scores[mhs[2]] + 1
        } else if (pcomps[comp] == 2) {
          scores[mhs[1]] <- scores[mhs[1]] + 1
        }
      }
      print(scores)
      scores
    }),
    best_ranked = map(ranks, function(rnk) {
      names(rnk[rnk == max(rnk)])
    })
  )


comparison <- results_with_ranks %>% 
  select(true_recs, pred_recs, best_ranked) %>% 
  mutate(pred_right = map2_lgl(true_recs, pred_recs, function(a, b) {all(b %in% a)}), 
         rank_right = map2_lgl(true_recs, best_ranked, function(a, b) {all(b %in% a)}),
         pred_conflict = map2_lgl(pred_recs, best_ranked, function(a, b) {all(b %in% a)}))
