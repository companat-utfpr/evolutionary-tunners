## Template for parameter description file for Iterated Race.
##
## The format is one parameter per line. Each line contains:
##
## 1: Name of the parameter. An unquoted alphanumeric string,
##    example: ants

## 2: Switch to pass the parameter. A quoted (possibly empty) string,
##    if the value and the switch must be separated, add a space at
##    the end of the string. Example : "--version1 --ants "

## 3: Type. An unquoted single letter, among
##     i: Integer, c: categorical, o: ordinal, r: real.

## 4: For c and o: All possible values, that is, a variable number of
##    quoted or unquoted strings separated by commas within
##    parenthesis. Empty strings and strings containing commas or
##    spaces must be quoted.
##    For i,r: a pair of unquoted numbers representing minimum and
##    maximum values.

## 5: A conditional parameter can be defined according to the values of
##    one or several other parameters.  This is done by adding a
##    character '|' followed by an R expression involving the names of
##    other parameters. This expression must return TRUE if the
##    condition is satisfied, FALSE otherwise.

# 1:            2:                   3: 4:      5:
ILS.Algo                          "" c (0,1,2,3)
Comp.Strat                        "" c (0,1)
Init.Strat                        "" c (0,1,2)
Neighborhood.Size                 "" r (0.0, 9.999)
Neighborhood.Strat                "" c (0,1)
ILS.Accept                        "" c (0,1,2)
LS.Single.Step                    "" c (0,1)
ILS.Accept.Temperature            "" r (0.0,5.0)    | ILS.Accept == 2
ILS.Perturb                       "" c (0,1,2)
ILS.Perturb.Restart.Init          "" c (0,1,2)      | ILS.Perturb == 0
ILS.Perturb.Restart.Threshold     "" i (0,10)       | ILS.Perturb == 0
ILS.Perturb.No.Kick               "" i (1,3)        | ILS.Perturb == 1
ILS.Perturb.Kick.Strength         "" r (0.0,1.0)    | ILS.Perturb == 1
ILS.Perturb.NILS.Escape           "" c (0,1,2,3,4)  | ILS.Perturb == 2
ILS.Perturb.NILS.MNS              "" i (0,1000)     | ILS.Perturb == 2
ILS.Perturb.NILS.Destruction.Size "" i (2,6)        | ILS.Perturb == 2 && ILS.Perturb.NILS.Escape == 3
ILS.Perturb.NILS.No.Kick          "" i (1,3)        | ILS.Perturb == 2 && ILS.Perturb.NILS.Escape == 4



