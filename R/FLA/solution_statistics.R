source("R/flowshop.R")
library(RFlowshopSolvers)
library(tidyverse)

initFactories("data")


problems <- read_csv("data/lower_bounds_data.csv") %>%
    filter(type == 'PERM', dist != 'taillard', no_jobs %in% c(10,20,30,50)) %>%
    mutate(
      stopping_criterium = 'EVALS',
      mh = 'dummy',
      budget = 'med'
    )

set.seed(12345)
solution_statistics_fla <- problems %>%
  mutate(
    No.Samples = 1000,
    seed = as.integer(runif(nrow(.), 0, 2147483647))
  ) %>%
  rowwise() %>%
  do({
    dt <- .
    print(paste(.$instance, .$objective))
    print(.$seed)
    solution_statistics <- solutionStatisticsFLA(
      rproblem = c(
        "problem" = .$problem,
        "instance" = .$instance,
        "type" = .$type,
        "objective" = .$objective,
        "budget" = .$budget,
        "stopping_criterium" = .$stopping_criterium
      ),
      rsampling = c(
        No.Samples = as.character(.$No.Samples)
      ),
      seed = .$seed
    )
    dt$fla_measure <- names(solution_statistics)
    dt$fla_value <- solution_statistics
    as_tibble(dt)
  }) %>% 
  unnest() %>%
  spread(fla_measure, fla_value) %>%
  mutate(
    total_edges = down + up + side,
    down = down / total_edges,
    up = up / total_edges,
    side = side / total_edges,
    iplat = iplat / No.Samples,
    ledge = ledge / No.Samples,
    lmax  = lmax  / No.Samples,
    lmin  = lmin  / No.Samples,
    slmax = slmax / No.Samples,
    slmin = slmin / No.Samples,
    slope = slope / No.Samples
  ) %>%
  select(-total_edges)

save(solution_statistics_fla, file = paste0("data/fla/solution_statistics_fla_all.Rdata"))
#solution_statistics_fla %>% unnest() %>% mutate() %>% group_by(fla_measure) %>% summarise(mean = mean(fla_value), sd = sd(fla_value))
