---
title: "R Notebook"
output:
  pdf_document: default
  html_notebook: default
---

```{r setup, message=F, echo=F}
knitr::opts_knit$set(root.dir = rprojroot::find_rstudio_root_file())
setwd(rprojroot::find_rstudio_root_file())
library(irace)
library(corrplot)
library(tidyverse)
source("R/flowshop.R")
source("R/models/model_utils.R")
DATA_FOLDER <- "data"
inputs <- inputMatrix(loadModelData("MH recommendation"), FALSE)
```

```{r corplot, fig.width=12, fig.height=12, echo=F, results="asis", fig.cap="Correlation among all metafeatures."}
library(corrplot)
mycorrplot <- function(dt) corrplot(cor(dt), type = "upper", order = "hclust", diag = F)
mycorrplot(inputs)
  # cat("\n\nRemoved inputs: ", caret::findCorrelation(cor(inputs), names = T), "\n\n")
```

```{r echo=FALSE, results='asis'}
  load("R/models/params_performances_dists.Rdata")
  #all_performances %>% filter(task == "classification", term %in% c("accuracy", "kappa"), mh == "IHC") %>% ggplot(aes(fill = term, y = estimate)) + geom_boxplot() + facet_wrap(~param, ncol = 4)
  regression_table <- all_performances %>% 
    filter(task == "regression",
           term %in% c("RMSE", "Rsquared")) %>% 
    group_by(mh, param, term) %>% 
    summarise(estimate = mean(estimate)) %>% 
    spread(term, estimate) %>%
    rename(
      Parameter = param,
      `$R^2$` = Rsquared
    ) %>%
    ungroup() %>%
    mutate(
      Parameter = case_when(
        mh == "ACO" & Parameter == "Local.Search"                      ~ "ACO local search (FI,HC,Random HC,IG)",
        mh == "ACO" & Parameter == "LS.Single.Step"                    ~ "ACO single step local search (yes,no)",
        mh == "IG"  & Parameter == "IG.Accept"                         ~ "IG acceptance (always,better,temperature)",
        mh == "IG"  & Parameter == "IG.Local.Search"                   ~ "IG local search (FI,HC,Random HC,IG)",
        mh == "IG"  & Parameter == "IG.LSPS.Local.Search"              ~ "IG partial solution local search (n/a,FI,HC,Random HC,IG)",
        mh == "IG"  & Parameter == "LSPS.Single.Step"                  ~ "IG single step partial local search (n/a,yes,no)",
        mh == "IHC" & Parameter == "HC.Algo"                           ~ "HC local search (FI,HC,Random HC,IG)",
        mh == "IHC" & Parameter == "Neighborhood.Size"                 ~ "HC neighborhood size (0-10\\%,...,90-100\\%)",
        mh == "IHC" & Parameter == "Neighborhood.Strat"                ~ "HC neighborhood strategy (ordered,random)",
        mh == "ILS" & Parameter == "ILS.Accept"                        ~ "ILS acceptance (always,better,temperature)",
        mh == "ILS" & Parameter == "ILS.Algo"                          ~ "ILS local search (FI,HC,Random HC or IG)",
        mh == "ILS" & Parameter == "ILS.Perturb"                       ~ "ILS perturb (restart,kick,NILS)",
        mh == "ILS" & Parameter == "ILS.Perturb.NILS.Destruction.Size" ~ "NILS perturb destruction size (n/a,2,...,6)",
        mh == "ILS" & Parameter == "ILS.Perturb.NILS.Escape"           ~ "NILS escape strategy (n/a,restart,destruction,kick)",
        mh == "ILS" & Parameter == "ILS.Perturb.NILS.No.Kick"          ~ "NILS number of kicks (n/a,1,2,3)",
        mh == "ILS" & Parameter == "ILS.Perturb.No.Kick"               ~ "ILS number of kicks (n/a,0-0.1*N,...,0.9-0.1*N)",
        mh == "ILS" & Parameter == "ILS.Perturb.Restart.Init"          ~ "ILS restart strategy (n/a,random,NEH,random NEH)",
        mh == "ISA" & Parameter == "Nb.Span.Max"                       ~ "SA max. number of span moves (n/a,2,...,10)",
        mh == "ISA" & Parameter == "Neighborhood.Size"                 ~ "SA neighborhood size (0-10\\%,...,90-100\\%)",
        mh == "TS"  & Parameter == "Neighborhood.Size"                 ~ "TS neighborhood size (0-10\\%,...,90-100\\%)",
        mh == "TS"  & Parameter == "Neighborhood.Strat"                ~ "TS neighborhood strategy (ordered,random)",
        mh == "TS"  & Parameter == "How.Long.Rnd.Taboo"                ~ "TS random taboo size (n/a,1,...,8)",
        mh == "TS"  & Parameter == "Max.Size.TL"                       ~ "TS taboo list maximum size (2,...,10)",
        TRUE ~ paste(mh, Parameter)
      )
    ) %>%
    select(-mh) %>%
    knitr::kable(format = "latex",
                 booktabs = TRUE,
                 escape = F,
                 digits = 2,
                 linesep = "") %>%
    print()
```




