library(tidyverse)
library(caret)
library(keras)
source("R/flowshop.R")

preProcessFSPCols <- function(configData, group_info = FALSE) {
  inst_n_col <- "inst_n"
  if (group_info)
    inst_n_col <- NULL
  configData <- configData %>%
    ungroup() %>%
    mutate_at(c("no_jobs", "no_machines", inst_n_col), as.integer) %>%
    mutate(
      instance = instanceNameForConfig(.)
    )
  for (col in c('data'))
    configData[col] <- NULL
  configData
}

preProcessFLA <- function(configData) {
  removeCols <- c(
    'stopping_criterium',
    'sample',
    'mh',
    'budget',
    'random_walk',
    'ensamble'
  )
  for (col in removeCols)
    configData[col] <- NULL
  configData
}

inputColsNames <- function(group_info = FALSE) {
  ic <- c(
    "budget", "dist", 
    "corr",
    "objective", "no_jobs", "no_machines", 
    "stopping_criterium", 
    "FI_fdc_adj", "FI_fdc_prec", "FI_fdc_abs_pos", 
    "FI_fdc_dev", "FI_fdc_shift", "FI_fdc_swap", "FI_mean_walk_length", 
    "HC_fdc_adj", "HC_fdc_prec", "HC_fdc_abs_pos", "HC_fdc_dev", 
    "HC_fdc_shift", "HC_fdc_swap", "HC_mean_walk_length", "IG_fdc_adj", 
    "IG_fdc_prec", "IG_fdc_abs_pos", "IG_fdc_dev", "IG_fdc_shift", 
    "IG_fdc_swap", "IG_mean_walk_length", "rw_autocorr_1", "rw_autocorr_2", 
    "rw_autocorr_4", "rw_cor_length", "entropy", "partial_inf", "inf_stability", "density_basin",
    "ratio"
  )
  if (group_info)
    ic <- c(ic, "inst_n")
  ic
}

loadModelData <- function(task, folder = "5000evals", tunner = "irace", group_info = FALSE) {
  DATA_FOLDER <- file.path("data")
  RESULTS_FOLDER <- file.path(DATA_FOLDER, "results", tunner, folder)
  
  load(file.path(DATA_FOLDER, "fla", "aw_fla_all.Rdata"))
  load(file.path(DATA_FOLDER, "fla", "rw_fla_all.Rdata"))
  load(file.path(RESULTS_FOLDER, "comparisons.Rdata"))
  aw_fla <- aw_fla %>% preProcessFSPCols(group_info) %>% preProcessFLA()
  rw_fla <- rw_fla %>% preProcessFSPCols(group_info) %>% preProcessFLA()
  model_data <- comparisons %>%
    preProcessFSPCols(group_info) %>%
    inner_join(aw_fla) %>%
    inner_join(rw_fla) %>%
    mutate(
      ratio = no_jobs / no_machines,
      rw_cor_length = 1 / log(abs(rw_autocorr_1))
    )
  if (task == "MH recommendation" && group_info) {
    cols_arr <- inputColsNames(T)
    model_data <- model_data %>%
      mutate(
        rec_ihc = map_int(best_mh_stat.group, ~ "IHC" %in% .x),
        rec_isa = map_int(best_mh_stat.group, ~ "ISA" %in% .x),
        rec_ts  = map_int(best_mh_stat.group, ~ "TS" %in% .x),
        rec_ils = map_int(best_mh_stat.group, ~ "ILS" %in% .x),
        rec_aco = map_int(best_mh_stat.group, ~ "ACO" %in% .x),
        rec_ig  = map_int(best_mh_stat.group, ~ "IG" %in% .x)
      ) %>%
      select(
        one_of(cols_arr),
        starts_with("rec"),
        -best_mh_mean,
        -best_mh_stat,
        -best_mh_ranks,
        -pairwise_comparisons
      )
  } else if (task == "MH recommendation") {
    cols_arr <- inputColsNames(group_info = group_info)
    model_data <- model_data %>%
      mutate(
        rec_ihc = map_int(best_mh_stat, ~ "IHC" %in% .x),
        rec_isa = map_int(best_mh_stat, ~ "ISA" %in% .x),
        rec_ts  = map_int(best_mh_stat, ~ "TS" %in% .x),
        rec_ils = map_int(best_mh_stat, ~ "ILS" %in% .x),
        rec_aco = map_int(best_mh_stat, ~ "ACO" %in% .x),
        rec_ig  = map_int(best_mh_stat, ~ "IG" %in% .x)
      ) %>%
      select(
        one_of(cols_arr),
        starts_with("rec"),
        -best_mh_mean,
        -best_mh_stat,
        -best_mh_ranks,
        -pairwise_comparisons
      )
  } else if (task == "MH mean rank regression") {
    model_data <- model_data %>% 
      select(-best_mh_mean, -best_mh_stat, -pairwise_comparisons) %>% 
      unnest(best_mh_ranks) %>%
      select(-fitness_mean) %>%
      mutate(mh = paste(mh, "rank")) %>%
      spread(mh, mh_rank) %>%
      select(
        one_of(inputColsNames(group_info)),
        ends_with("rank")
      )
  } else if (task == "MH pairwise ranking") {
    model_data <- model_data %>%
      select(-best_mh_mean, -best_mh_ranks) %>% 
      unnest(pairwise_comparisons) %>% 
      mutate(
        comp = if_else(p.value < 0.05, mean_compare * 2 - 1, 0),
        mhs = paste(AlgoA, "x", AlgoB)) %>%
      select(-AlgoA, -AlgoB, -mean_compare, -p.value) %>%
      spread(mhs, comp)
  } else if (task == "Parameter recommendation") {
    load(file.path(RESULTS_FOLDER, "params_data.Rdata"))
    params_data <- params_data %>% preProcessFSPCols(group_info)
    model_data <- model_data %>%
      full_join(params_data, by = c("problem", "dist", "corr", "type", "objective", "no_jobs", "no_machines", "inst_n", "instance", "budget", "stopping_criterium"))
  } else if (task == "Parameter distribution") {
    load(file.path(RESULTS_FOLDER, "params_histograms.Rdata"))
    params_histograms <- params_histograms %>%
      preProcessFSPCols(group_info)
    model_data <- model_data %>%
      full_join(params_histograms, by = c("problem", "dist", "corr", "type", "objective", "no_jobs", "no_machines", "inst_n", "instance", "budget", "stopping_criterium"))
  } else {
    stop(paste("Unknown task", task, " =("))
  }
  model_data %>%
    mutate_if(is.logical, as.integer) %>%
    mutate_if(is.character, factor) %>%
    as.data.frame()
}

preProcessData <- function() {
  model_data <- loadModelData("MH recommendation", tunner = "irace_groups", group_info = T)
  save(model_data, file = "data/models/MHRecommendationIraceGroups.Rdata")
}

# preProcessData()

getInputCols <- function(modelData, group_info = FALSE) {
  modelData[, inputColsNames(group_info)]
}

removeCorrelatedFeatures <- function(modelData) {
  cors <- cor(modelData)
  correlated_inputs <- caret::findCorrelation(cors, verbose = T)
  cat("Removed cols: ", colnames(modelData)[correlated_inputs])
  modelData[,-correlated_inputs]
}

setDummyVariables <- function(model_inputs) {
  discrete_cols <- names(model_inputs)[sapply(model_inputs, is.factor)]
  numeric_cols <- names(model_inputs)[sapply(model_inputs, is.numeric)]
  
  input_matrix <- as.matrix(model_inputs[,numeric_cols])
  for (cl in discrete_cols) {
    # vals <- as.integer(model_inputs[,cl]) - 1
    # nc <- n_distinct(vals)
    # 
    # vals <- to_categorical(vals, num_classes = nc)[,1:(nc-1)] %>% as.matrix()
    vals <- caret::class2ind(model_inputs[,cl], drop2nd = F)
    # vals <- vals[,1:(ncol(vals) - 1)]
    colnames(vals) <- paste0(cl, ".", 1:ncol(vals))
    input_matrix <- cbind(input_matrix[], vals)
  }
  
  input_matrix
}

inputMatrix <- function(modelData, removeCorrelations = T) {
  model_inputs <- getInputCols(modelData)
  input_matrix <- setDummyVariables(model_inputs)
  if (removeCorrelations) {
    removeCorrelatedFeatures(input_matrix)
  } else {
    input_matrix
  }
}

outputMatrix <- function(modelData, task) {
  if (task == "MH recommendation") {
    modelData %>% 
      dplyr::select(contains("rec_")) %>%
      as.matrix()
  } else if (task == "MH mean rank regression") {
    modelData %>% 
      dplyr::select(contains("rank")) %>%
      as.matrix()
  } else if (task == "MH pairwise ranking") {
    modelData %>% 
      dplyr::select(contains(" x ")) %>%
      as.matrix()
  } else {
    stop()
  }
}

normalizeColumn <- function(x) (x - min(x)) / (max(x) - min(x))

normalizeMatrix <- function(M, low = -1, high = 1) {
  apply(M, 2, normalizeColumn) * (high - low) + low
}

clampColumn <- function(x, low = 0, high = 1) {
  pmax(low, pmin(high, x))
}

clampMatrix <- function(M, low = 0, high = 1) {
  apply(M, 2, clampColumn, low, high)
}


trainModel <- function(model_name,
                       train_input_data,
                       train_output_data,
                       test_input_data,
                       test_output_data) {
  # library(doParallel)
  # cl <- makePSOCKcluster(3)
  # registerDoParallel(cl)
  
  time <- system.time(
    train_result <- train(
      x = train_input_data,
      y = train_output_data,
      method = model_name,
      metric = 'Kappa',
      trControl = trainControl(
        method = 'cv',
        number = 10,
        classProbs = F
      )
    )
  )
  
  # stopCluster(cl)
  
  final_model <- train_result$finalModel
  predicted_test_output <- predict(train_result, test_input_data, type = "raw")
  perf <- as.list(postResample(predicted_test_output, test_output_data))
  perf$Precision <- MLmetrics::Precision(test_output_data, predicted_test_output)
  perf$Recall <- MLmetrics::Recall(test_output_data, predicted_test_output)
  perf$F1 <- MLmetrics::F1_Score(test_output_data, predicted_test_output)
  cm <- confusionMatrix(predicted_test_output, test_output_data)
  print(time)
  
  tibble(
    train_result = list(train_result),
    model = list(final_model),
    confusion_matrix = list(cm),
    performance = list(perf),
    predicted = list(predicted_test_output),
    reference = list(test_output_data),
    time = list(time)
  )
}

nestedCVtrainRecommendation <- function(model_name, mh, preprocess = NULL) {
  if (is.null(preprocess)) {
    preprocess <- getInputCols
  }
  set.seed(123)
  recommendation_data <- loadModelData("MH recommendation")
  input_data <- preprocess(recommendation_data)
  mh_rec <- mh
  out_col_name <- str_to_lower(paste0('rec_', mh_rec))
  output_data <- factor(recommendation_data[,out_col_name], 
                        labels = c('no', 'yes'))
  train_idxs_folds <- createFolds(
    output_data, 
    k = 10,
    returnTrain = T
  )
  i <- 1
  map_dfr(train_idxs_folds, function(train_idxs) {
    cat(mh, i, '\n')
    i <<- i + 1
    trainModel(
      model_name,
      input_data[train_idxs,],
      output_data[train_idxs],
      input_data[-train_idxs,],
      output_data[-train_idxs]
    )
  }, .id = "fold")
}


trainPairwiseRankingModel <- function(model_name,
                       train_input_data,
                       train_output_data,
                       no_procs,
                       i) {
  library(doParallel)
  cl <- makePSOCKcluster(no_procs)
  registerDoParallel(cl)
  
  models <- map(1:ncol(train_output_data), function(i) {
    y <- factor(train_output_data[,i], levels = c(-1,0,1), labels =  c('worse', 'equal', 'better'))
    train(
      x = train_input_data,
      y = y,
      method = model_name,
      metric = 'Kappa',
      trControl = trainControl(
        method = 'cv',
        number = 10,
        classProbs = F
      )
    )
  })
  
  stopCluster(cl)
  
  # fn <- sprintf("05_ranking_models_%s_fold_%d_models.Rdata", model_name, i)
  # save(models, file = fn)
  # remove(models)
  # fn
  models
}

nestedCVtrainPairwiseRankingModel <- function(model_name, task = "MH pairwise ranking", preprocess = NULL, no_procs = 1) {
  if (is.null(preprocess)) {
    preprocess <- getInputCols
  }
  set.seed(123)
  ranking_data <- loadModelData(task)
  input_data <- preprocess(ranking_data)
  output_data <- outputMatrix(ranking_data, task)
  train_idxs_folds <- createFolds(
    1:nrow(output_data), 
    k = 10,
    returnTrain = T
  )
  i <- 1
  map_dfr(train_idxs_folds, function(train_idxs) {
    cat(model_name, 'fold ', i, '\n')
    i <<- i + 1
    list(
      models = list(trainPairwiseRankingModel(
        model_name,
        input_data[train_idxs,],
        output_data[train_idxs,],
        no_procs,
        i
      )),
      test_input_data = list(input_data[-train_idxs,]),
      test_output_data = list(output_data[-train_idxs,])
    )
  }, .id = "fold")
}



