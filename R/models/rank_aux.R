source("R/flowshop.R")
source("R/models/model_utils.R")
library(mldr)


dcg_score <- function(y_true, y_score, k=10, gains="exponential") {
  od <- rev(order(y_score))
  y_true <- y_true[od[1:min(length(od),k)]]
  if (gains == "exponential")
    gains <- 2 ** y_true - 1
  else
    gains <- y_true
  discounts <- log2(0:(length(y_true) - 1) + 2)
  sum(gains / discounts)
}

ndcg_score <- function(y_true, y_score, k=10, gains="exponential") {
  best <- dcg_score(y_true, y_true, k, gains)
  actual <- dcg_score(y_true, y_score, k, gains)
  actual / best
}

# dcg_score(c(5, 3, 2), c(2, 1, 0)) > dcg_score(c(4, 3, 2), c(2, 1, 0))
# dcg_score(c(4, 3, 2), c(2, 1, 0)) > dcg_score(c(1, 3, 2), c(2, 1, 0))
# ndcg_score(c(5, 3, 2), c(2, 1, 0)) == 1.0
# ndcg_score(c(2, 3, 5), c(0, 1, 2)) == 1.0
# ndcg_score(c(5, 3, 2), c(2, 1, 0), k=2) == 1.0
# ndcg_score(c(2, 3, 5), c(0, 1, 2), k=2) == 1.0
# dcg_score(c(5, 3, 2), c(2, 1, 0)) == dcg_score(c(2, 3, 5), c(0, 1, 2))
# dcg_score(c(5, 3, 2), c(2, 1, 0), k=2) == dcg_score(c(2, 3, 5), c(0, 1, 2), k=2)


cols2ranks <- function(...) {
  n <- length(ALL_MHS)
  scores <- rep(0, n)
  names(scores) <- ALL_MHS
  pcomps <- list(...)
  for (comp in names(pcomps)) {
    mhs <- str_split(comp, " x ", simplify = T)
    if (pcomps[comp] == 'worse') {
      scores[mhs[2]] <- scores[mhs[2]] + 1
    } else if (pcomps[comp] == 'better') {
      scores[mhs[1]] <- scores[mhs[1]] + 1
    }
  }
  rank(n - scores, ties.method = 'average')
}


rec_data <- loadModelData("MH recommendation")
evaluateFold <- function(fold, models, test_input_data, test_output_data, preprocess = function(x) x, multioutput = FALSE) {
  comps <- colnames(test_output_data)
  names(models) <- comps
  predictions <- map_dfc(models, function(md) {
    if (multioutput) {
      mt <- predict(md$finalModel, preprocess(test_input_data))
      mt <- matrix(mt, nrow = 3, ncol = length(mt)/3) %>% 
        t() %>%
        data.frame() %>%
        mutate(max_prob = max.col(., "last"))
      factor(mt$max_prob, levels = c(1,2,3), labels = c("worse", "equal", "better"))
    } else {
      predict(md, preprocess(test_input_data), type = "raw")
    }
  })
  predictions_ranks <- predictions %>%
    mutate_all(as.character) %>%
    mutate(ranks = pmap(., cols2ranks)) %>% 
    select(ranks) %>%
    mutate(mh = map(ranks, names)) %>% 
    rowid_to_column() %>% 
    unnest() %>% 
    select(rowid, ranks, mh) %>% 
    group_by(rowid) %>% 
    spread(mh, ranks) %>%
    ungroup() %>%
    select(-rowid)
  
  test_rec <- test_input_data %>% 
    left_join(rec_data, by = c("budget", "dist", "corr", "objective", "no_jobs", "no_machines", "stopping_criterium", "FI_fdc_adj", "FI_fdc_prec", "FI_fdc_abs_pos", "FI_fdc_dev", "FI_fdc_shift", "FI_fdc_swap", "FI_mean_walk_length", "HC_fdc_adj", "HC_fdc_prec", "HC_fdc_abs_pos", "HC_fdc_dev", "HC_fdc_shift", "HC_fdc_swap", "HC_mean_walk_length", "IG_fdc_adj", "IG_fdc_prec", "IG_fdc_abs_pos", "IG_fdc_dev", "IG_fdc_shift", "IG_fdc_swap", "IG_mean_walk_length", "rw_autocorr_1", "rw_autocorr_2", "rw_autocorr_4", "rw_cor_length", "entropy", "partial_inf", "inf_stability", "density_basin", "ratio")) %>%
    select(starts_with("rec_")) %>%
    rename_all(~str_to_upper(str_extract(.x, "[a-z]+$")))
  # average_precision(ref_dt, pred_dt, ties_method = "average")
  
  predictions_matrix <- -as.matrix(predictions_ranks[,ALL_MHS])
  test_matrix <- as.matrix(test_rec[,ALL_MHS])
  
  list(
    average_precision = mldr::average_precision(test_matrix, predictions_matrix),
    one_error = mldr::one_error(test_matrix, predictions_matrix),
    coverage = mldr::coverage(test_matrix, predictions_matrix),
    ranking_loss = mldr::ranking_loss(test_matrix, predictions_matrix),
    macro_auc = mldr::macro_auc(test_matrix, predictions_matrix),
    micro_auc = mldr::micro_auc(test_matrix, predictions_matrix),
    example_auc = mldr::example_auc(test_matrix, predictions_matrix)
  )
}

bestMean <- function(dt, metric) {
  sum_func <- max
  if (metric %in% c('one_error', 'coverage', 'ranking_loss'))
    sum_func <- min
  dt %>% 
    group_by(model) %>%
    filter(!is.na(value)) %>%
    summarize(mean_val = mean(value)) %>% 
    filter(mean_val == sum_func(mean_val)) %>% 
    pull(model)
}

makeSymm <- function(m) {
  m[upper.tri(m)] <- t(m)[upper.tri(m)]
  return(m)
}

tiesWithBest <- function(dt, best_mean) {
  # normality_test <- dt %>% 
  #   group_by(model) %>%
  #   nest() %>%
  #   mutate(is_normal = map_lgl(data, ~shapiro.test(.x$value)$p.value > 0.05))
  # if (!all(normality_test$is_normal)) {
  #   print("some metrics are not normal!")
  # }
  phtest <- PMCMRplus::frdAllPairsNemenyiTest(dt$value, dt$model, dt$fold)
  p_val_matrix <- makeSymm(phtest$p.value)
  p_values = as.data.frame(as.table(p_val_matrix))
  colnames(p_values) <- c("AlgoA", "AlgoB", "p.value")
  p_values <- p_values %>%
    mutate_if(is.factor, as.character)
  ties_with_winner <- p_values %>% 
    filter(AlgoA %in% best_mean | AlgoB %in% best_mean, p.value > 0.05)
  unique(c(best_mean, ties_with_winner$AlgoA, ties_with_winner$AlgoB))
}

formatedVals <- function(model, mv, sv, best_mean, ties_with_best, ...) {
  print(mv)
  res <- sprintf("%.2f (%.2f)", mv, sv)
  if (model %in% best_mean) {
    res <- paste0("<strong>", res, "</strong>")
  }
  if (model %in% ties_with_best) {
    res <- paste0("<i>", res, "</i>")
  }
  res
}

performances_fn <- "05_ranking_models_performances.Rdata"
if (!file.exists(performances_fn)) {
  load("05_ranking_models_rpart_models.Rdata")
  rpartPerf <- models %>%
    mutate(performances = pmap(., evaluateFold)) %>%
    select(fold, performances) %>%
    mutate(model = "rpart")
  
  load("05_ranking_models_rf_models.Rdata")
  rfPerf <- models %>%
    mutate(performances = pmap(., evaluateFold)) %>%
    select(fold, performances) %>%
    mutate(model = "rf")
  
  load("05_ranking_models_xgbTree_models.Rdata")
  xgbPerf <- models %>%
    mutate(performances = pmap(., evaluateFold, preprocess = function(x) inputMatrix(x, FALSE)), multioutput = TRUE) %>%
    select(fold, performances) %>%
    mutate(model = "xgb")
  rm(models)
  performances <- bind_rows(rpartPerf, rfPerf, xgbPerf)
  save(performances, file = performances_fn)
}
load(performances_fn)

performances_tidy <- performances %>%
  select(model, fold, performances) %>%
  mutate(
    performances = map(performances, as.data.frame)
  ) %>%
  unnest() %>%
  gather(metric, value, -model, -fold)

test_dt <- performances_tidy %>%
  group_by(metric) %>%
  nest() %>%
  mutate(
    best_mean = map2(data, metric, bestMean),
    ties_with_best = map2(data, best_mean, tiesWithBest)
  )

summ_performances <- performances_tidy %>%
  group_by(model, metric) %>%
  summarise(
    mv = mean(value, na.rm = T),
    sv = sd(value, na.rm = T)
  ) %>%
  left_join(test_dt, by = c("metric")) %>%
  mutate(
    is_best = map2_lgl(model, best_mean, ~ .x %in% .y),
    is_equal = map2_lgl(model, ties_with_best, ~ .x %in% .y),
    formated = case_when(
      is_best ~ sprintf("\\textbf{%.2f (%.2f)}", mv, sv),
      is_equal ~ sprintf("\\textit{%.2f (%.2f)}", mv, sv),
      TRUE ~ sprintf("%.2f (%.2f)", mv, sv)
    )
  ) %>%
  select(model, metric, formated)

summ_performances %>%
  ungroup() %>%
  spread(metric, formated) %>%
  as.matrix() %>%
  t() %>%
  as.data.frame() %>%
  knitr::kable(format = 'latex', escape = F, booktabs = T)
