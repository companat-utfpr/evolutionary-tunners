library(keras)
library(caret)
library(broom)
source("R/models/model_utils.R")

set.seed(123456)
use_session_with_seed(123456)

task <- "MH recommendation"
model_data <- loadModelData(task)
input_matrix <- inputMatrix(model_data) %>% 
  normalizeMatrix(-1, 1)
output_matrix <- outputMatrix(model_data, task) %>%
  normalizeMatrix(0, 1)

folds <- createFolds(output_matrix[,1], k = 5)

performances <- NULL
mh_performances <- NULL
fold_i <- 1
k_clear_session()
for (fold in folds) {
  x_train <- input_matrix[-fold,]
  y_train <- output_matrix[-fold,] %>% as.matrix()
  x_test <- input_matrix[fold,]
  y_test <- output_matrix[fold,] %>% as.matrix()
  
  no_inputs <- ncol(x_train)
  no_outputs <- ncol(y_train)
  model <- keras_model_sequential() 
  model %>% 
    layer_dense(input_shape = no_inputs,
                units = no_outputs,
                activation = 'linear')
  summary(model)
  
  model %>% compile(
    loss = "mse",
    optimizer = optimizer_sgd(),
    metrics = rep('binary_accuracy', 1)
  )
  
  ties <- y_train %>% apply(1, sum) == 0
  sample_weight <- rep(1, nrow(y_train))
  sample_weight[ties] <- 0.5
  history <- model %>% fit(
    x_train, y_train,
    epochs = 300, batch_size = 128, 
    validation_split = 0.0,
    sample_weight = sample_weight
    # ,callbacks = list(callback_early_stopping(monitor = 'val_loss'))#,
    # ,class_weight = list(`0` = 2, `1` = 1)
  )
  
  prediction <- round(clampMatrix(predict(model, x_test), 0, 1))
  colnames(prediction) <- colnames(y_test)
  cm <- caret::confusionMatrix(as.factor(prediction), as.factor(y_test))
  performances <- c(performances, cm)
  k_clear_session()
  
  for (mh_idx in 1:length(ALL_MHS)) {
    dt <- tidy(caret::confusionMatrix(as.factor(prediction[,mh_idx]), 
                                          as.factor(y_test[,mh_idx])))
    dt$fold <- fold_i
    dt$mh <- colnames(y_test)[mh_idx]
    mh_performances <- bind_rows(mh_performances, dt)
  }
  fold_i <- fold_i + 1
}
performances[c(3,9,15,21,27)] %>% 
  data.frame() %>%
  t() %>%
  data.frame() %>%
  summarise(
    mean_acc = mean(Accuracy),
    mean_kappa = mean(Kappa)
  ) %>%
  print()

mh_performances %>% filter(term %in% c("accuracy", "kappa")) %>% group_by(mh,term) %>% summarise(estimate = mean(estimate))
