library(RFlowshopSolvers)
source("R/flowshop.R")
initFactories("data")

enumerated_small_instances <- getConfigsFromCmd(
  c("--no_jobs", "10",
    "--budget", "med",
    "--mh", "dummy",
    "--stopping_criterium", "EVALS")
) %>%
  mutate(
    instance = instanceNameForConfig(.)
  )

enumerated_small_instances$counts <- apply(configs, 1, function(config) {
  rproblem <- c(
    "problem" = config['problem'],
    "instance" = config['instance'],
    "type" = config['type'],
    "objective" = config['objective'],
    "budget" = config['budget'],
    "stopping_criterium" = config['stopping_criterium']
  )
  print(config)
  data_frame(
    fitness = enumerateAllFitness(config)
  ) %>%
    count(fitness)
})

save(enumerated_small_instances, file = "data/enumerated_small_instances.Rdata")

