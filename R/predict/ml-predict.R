library(tidyverse)
library(irace)
library(RFlowshopSolvers)
source("R/flowshop.R")
source("R/group_experiments/common.R")

initFactories(file.path("data"))

specs <- map(ALL_MHS, function(mh) {
  readParameters(file.path("data", "specs", paste0(mh, ".txt")))
})
names(specs) <- ALL_MHS

randomConfig <- function(...) {
  row <- list(...)
  pn <- specs[[row$pred_mh]]$names
  params <- as.numeric(row[pn])
  names(params) <- pn
  params
}

randomMH <- function(...) {
  list(...)$pred_mh
}

evaluatePrediction <- function(...) {
  sample <- list(...)
  if ('seed' %in% attributes(sample)$names) {
    seeds <- sample$seed
  } else {
    seeds <- as.integer(runif(30, 1, 9999999))
  }
  print(sample$config)
  rproblem <- as.character(sample[problem_cols])
  names(rproblem) <- problem_cols
  map(seeds, function(seed) {
    solveFSP(
      rproblem = rproblem,
      mh = sample$mh,
      seed = seed,
      rparams = sample$config,
      verbose = F
    )  
  }) %>% 
    map_dfr(as.data.frame)
}

datasetPerf <- function(dt) {
  dt <- dt %>% left_join(problems) %>%
    mutate(
      instance = instanceNameForConfig(.)
    )
  dt %>%
    mutate(mh = pmap_chr(., randomMH)) %>%
    mutate(config = pmap(., randomConfig)) %>%
    mutate(perf = pmap(., evaluatePrediction))
}

problem_cols <- c("problem", "dist", "corr", "type", "objective",
                  "no_jobs", "no_machines", "inst_n", "instance",
                  "budget", "stopping_criterium")

problems <- read_csv(file.path('data', 'models', 'prediction', 'problems.csv'))

dataset_i <- 1
algo <- 'ns_hist_rf_multilabel_mh_recommendation_pp'
dataset_type <- 'test'
if (length(commandArgs(T)) > 0) {
  dataset_i <- as.integer(commandArgs(T)[1])
  algo <- commandArgs(T)[2]
  dataset_type <- commandArgs(T)[3]
}
dataset_fn <- file.path('data', 'models', 'prediction',
                        paste0('dataset_', dataset_i, '.csv'))
dataset <- read_csv(dataset_fn)

if (dataset_type == "test") {
  dataset <- getTestDt(dataset)
} else {
  dataset <- getTrainDt(dataset)
}
predicts <- read_csv(file.path("data", "models", "prediction", paste0(algo, "_predicts.csv")))
dataset <- dataset %>%
  inner_join(predicts)
dataset_results <- datasetPerf(dataset)

save(dataset_results,
    file = file.path('data', 'models', 'prediction', 
                     paste0('dataset_', dataset_i, '_', algo,
                            if_else(dataset_type == 'train', '_train', ''),
                            '.Rdata')))

