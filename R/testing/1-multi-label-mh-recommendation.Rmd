---
title: "Multi-label MH Recommendation"
author: "Lucas MP"
date: "21 de outubro de 2019"
output: 
  pdf_document:
    keep_tex: TRUE
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(warning = FALSE, 
                      message = FALSE)
library(tidyverse)
library(scmamp)
# library(rpart.plot)
source(here::here("R/flowshop.R"))

if (!("Rgraphviz" %in% installed.packages())) {
  install.packages("BiocManager")
  BiocManager::install("Rgraphviz")
}

loadTaskData <- function(task) {
  best_perfs_fn <- here::here("data", "models", task, "best_perfs_one-to-many.Rdata")
  load(best_perfs_fn)
  all_algoritms
}

loadMHRecommendationData <- function(task, print) {
  loadTaskData(task) %>% mutate(print = print)
}

loadAllMHRecommendationsData <- function() {
  # load("data/models/multilabel_mh_recommendation/extra_multilabel_mh_recommendation.Rdata")
  # dt1e <- all_algoritms
  # dt1e$feature_selection <- 'Without feature selection'
  # load("data/models/multilabel_mh_recommendation_feature_selection/extra_multilabel_mh_recommendation_feature_selection.Rdata")
  # dt2e <- all_algoritms
  # dt2e$feature_selection <- 'With feature selection'
  bind_rows(
    loadMHRecommendationData("multilabel_mh_recommendation", "Instance") #,
    # loadMHRecommendationData("multilabel_mh_recommendation_feature_selection", "Instance FS"),
    # loadMHRecommendationData("multilabel_group_mh_recommendation", "Groups")
    # loadMHRecommendationData("multilabel_group_mh_recommendation_feature_selection", "Groups FS")
  )
}
perf_data <- loadAllMHRecommendationsData() %>%
  select(-params) %>%
  mutate(strategy = str_to_upper(strategy))

all_data <- perf_data %>%
  mutate(perfs = map(perfs, ~enframe(.x$perf$multilabel))) %>%
  unnest(cols = c(perfs))

all_data_label <- perf_data %>%
  mutate(
    perfs = map(perfs, ~as_tibble(.x$perf$labels) %>%
                  mutate(mh = rownames(.x$perf$labels)))
  ) %>%
  unnest(cols = c(perfs)) %>%
  mutate(mh = str_replace(mh, "rec_", ""),
         mh = str_to_upper(mh),
         mh = factor(mh, labels = ALL_MHS))

all_data %>% select(strategy, base.algorithm, dataset, name, value) %>% write_csv("~/Dropbox/supplementary-material-paper-2019/data/mh_recommendation_perf.csv")
```

## Chosing the best inner algorithm

```{r data-compare-inner-models, echo=FALSE}
dt_best_inner_algorithm <- all_data %>%
  filter(print == 'Instance')
```

```{r}
dt_best_inner_algorithm %>%
  filter(name %in% c('macro-AUC', 'average-precision')) %>%
  group_by(strategy, name) %>%
  mutate(rk = rank(value)) %>%
  group_by(strategy, name, base.algorithm) %>%
  summarise(mean_rank = mean(rk)) %>%
  pivot_wider(id_cols = c(strategy, name),
              names_from = base.algorithm,
              values_from = mean_rank) %>%
  ungroup() %>%
  arrange(name)
```


```{r boxplot-compare-inner-models, echo=FALSE, fig.asp=.2}
dt_best_inner_algorithm%>%
  filter(name %in% c('macro-AUC', 'average-precision')) %>%
  ggplot(aes(x = base.algorithm, y = value, fill = base.algorithm)) +
  facet_wrap(~name, scales = 'free_x') +
  geom_boxplot() +
  coord_flip() +
  scale_fill_viridis_d() +
  labs(x = NULL, y = NULL, fill = NULL) +
  theme_bw() +
  theme(legend.position = 'none')
```


```{r cd-compare-inner-models-cd, echo=FALSE}
plotFriedmanCdByInnerModel <- function(ml_metrics, metric) {
  test_dt <- ml_metrics %>%
    filter(name %in% c(metric)) %>%
    mutate(sample = paste(str_extract(dataset, "dataset_.?"), print, name, strategy),
           value = if_else(is.na(value), 0, value))
  dt <- test_dt %>% 
    select(base.algorithm, sample, value) %>% 
    spread(base.algorithm, value) %>% 
    column_to_rownames("sample") %>%
    as.matrix()
  # test <- postHocTest(dt, test="friedman", correct="bergmann", use.rank=TRUE)
  # plotRanking(pvalues=test$corrected.pval, summary=test$summary, alpha=0.05)
  scmamp::plotCD(dt, 0.045, cex = 1.5)
}

plotFriedmanCdByInnerModel(dt_best_inner_algorithm, 'macro-AUC')
plotFriedmanCdByInnerModel(dt_best_inner_algorithm, 'average-precision')
```

```{r}
best_algorithm <- 'RF'
```


## Choosing the best multi-label strategy

```{r compare-multi-label-strategies-data, echo=FALSE}
dt_ml_strategies <- all_data %>%
  filter(base.algorithm == best_algorithm,
         print == 'Instance')
```

```{r boxplot-compare-multilabel-strategies, echo=FALSE}
# dput(unique(dt_ml_strategies$strategy))

plt <- dt_ml_strategies %>%
  filter(name %in% c('macro-AUC', 'average-precision')) %>%
  mutate(strategy = factor(
    strategy,
    levels = c(
      # Transformations for Identifying Label Dependences
      "PRUDENT", "CTRL", "BRPLUS", "RDBR", "DBR", "MBR", "NS", "ECC", "CC", 
      # Pairwise methods.
      "RPC", "CLR",
      # Label powerset methods.
      "HOMER", "RAKEL", "LP", 
      # Binary relevance methods.
      "ESL", "EBR", "BR"
    )                    
  )) %>%
  ggplot(aes(x = strategy, y = value, fill = strategy)) +
  geom_boxplot() +
  facet_wrap(~name, scales = "free_x") +
  labs(x = NULL, y = NULL) +
  coord_flip() +
  theme_bw() +
  theme(legend.position = 'none', legend.title = element_blank()) +
  scale_fill_viridis_d()
# ggsave(
#   "rec-mh.pdf",
#   plt,
#   width = 7,
#   height = 10,
#   dpi = 300,
#   units = 'in'
# )
plt
```

```{r cd-compare-multilabel-strategies, echo=FALSE}
plotFriedmanCd <- function(ml_metrics, metric) {
  test_dt <- ml_metrics %>%
    filter(name %in% c(metric)) %>%
    mutate(sample = paste(str_extract(dataset, "dataset_.?"), print, name, base.algorithm),
           value = if_else(is.na(value), 0, value))
  dt <- test_dt %>% 
    select(strategy, sample, value) %>% 
    spread(strategy, value) %>% 
    column_to_rownames("sample") %>%
    as.matrix()
  scmamp::plotCD(dt, cex = 0.50, vfont = 'Times')
}

plotFriedmanCd(dt_ml_strategies, 'macro-AUC')
plotFriedmanCd(dt_ml_strategies, 'average-precision')
# kwCd(ml_metrics, 'macro-AUC')
# kwCdBa(ml_metrics, 'average-precision')
# kwCdBa(ml_metrics, 'macro-AUC')
# 
# kwCd(ml_metrics %>% filter(print == 'Groups'), 'average-precision')
```


```{r}
best_strategy <- 'BR'
```



```{r data-group-compare, echo=F}
# dt_ml_group_strategies <- all_data %>%
#   filter(strategy == best_strategy,
#          base.algorithm == best_algorithm)
```

```{r boxplot-group-data-compare, echo=FALSE, fig.asp=.18}
# dt_ml_group_strategies %>%
#   filter(name %in% c('average-precision', 'macro-AUC')) %>%
#   ggplot(aes(x = print, y = value, fill = print)) +
#   facet_wrap(~name, scales = 'free_x') +
#   geom_boxplot() +
#   coord_flip() +
#   scale_fill_viridis_d() +
#   labs(x = NULL, y = NULL, fill = NULL) +
#   theme_bw() +
#   theme(legend.position = 'none')
```

```{r cd-group-data-compare, echo=FALSE}
# plotFriedmanCdByGroup <- function(dt_ml_group_strategies, metric) {
#   test_dt <- dt_ml_group_strategies %>%
#     filter(name %in% c(metric)) %>%
#     mutate(sample = paste(str_extract(dataset, "dataset_.?"), name, strategy, base.algorithm),
#            value = if_else(is.na(value), 0, value))
#   dt <- test_dt %>% 
#     select(print, sample, value) %>% 
#     spread(print, value) %>% 
#     column_to_rownames("sample") %>%
#     as.matrix()
#   scmamp::plotCD(dt, cex = 0.8, vfont = 'Times')
# }
# 
# plotFriedmanCdByGroup(dt_ml_group_strategies, 'macro-AUC')
# plotFriedmanCdByGroup(dt_ml_group_strategies, 'average-precision')
```

```{r}
best_dataset <- "Instance"
```


## Label metrics for the best multi-label recommendation

```{r boxplot-per-label, fig.asp=.3}
all_data_label %>% 
  filter(
    base.algorithm == best_algorithm,
    strategy == best_strategy,
    print == best_dataset
  ) %>%
  select(mh, precision, recall) %>%
  gather(key = metric, value = value, precision, recall) %>%
  mutate(
    mh = case_when(
        as.character(mh) == 'IHC' ~ 'HC',
        as.character(mh) == 'ISA' ~ 'SA',
        T ~ as.character(mh)
    ),
      #labels = rev(c('HC','SA','TS','ACO','ILS','IG'))
    # ),
    metric = str_to_upper(metric)
  ) %>%
  mutate(mh = factor(mh, labels = rev(c('HC','SA','TS','ACO','ILS','IG')),
                     levels = rev(c('HC','SA','TS','ACO','ILS','IG')))) %>%
  ggplot(aes(x = mh, y = value, fill = mh)) +
  facet_wrap(~metric, scales = 'free_x') +
  geom_boxplot() +
  coord_flip() +
  scale_fill_viridis_d() +
  labs(x = NULL, y = NULL, fill = NULL) +
  theme_bw() +
  theme(legend.position = 'none')
```

```{r cart-example}
# dt <- read_csv("data/models_0911/multilabel_mh_recommendation_feature_selection/dataset_1.csv")
# train_dt <- dt %>%
#   rename(output = rec_ts) %>%
#   mutate(output = factor(output)) %>%
#   select(-starts_with('rec_'), -folds)
# md <- caret::train(
#   output ~ .,
#   data = train_dt,
#   'rpart',
#   tuneGrid = expand.grid(
#     cp = c(0.02)
#   )
# )
# # pdf("cart-ts-rec.pdf", width = 3, height = 2,
# #     family = 'Times', pointsize = 8)
# # par(mar=c(.5,.5,.5,.5)-.5)
# rpart.plot(md$finalModel, type = 3, fallen.leaves = T, extra = 100)
# dev.off()
```


```{r feature-importance-example}
# 
# rf_imps <- map2_dfr(model$models, model$labels, 
#                     ~ as.data.frame(varImp(.x)) %>% 
#                       rownames_to_column() %>%
#                       mutate(mh = .y))
# plt <- rf_imps %>%
#   mutate(mh = str_to_upper(str_extract(mh, "[a-z]*$"))) %>%
#   group_by(mh, rowname) %>%
#   mutate(total = sum(Overall)) %>%
#   mutate(order = rank(total)) %>%
#   transform(rowname = reorder(rowname, total)) %>%
#   ggplot() +
#   geom_col(aes(fill = mh, x = rowname, y = Overall)) +
#   coord_flip() +
#   labs(x = NULL, y = "importance", fill = "MH") +
#   theme_bw()
# plt
# ggsave(
#   "feature-importance.pdf",
#   plt,
#   width = 150,
#   height = 150,
#   dpi = 300,
#   units = 'mm'
# ) 
```
