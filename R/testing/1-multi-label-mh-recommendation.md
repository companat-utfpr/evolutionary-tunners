---
title: "Multi-label MH Recommendation"
author: "Lucas MP"
date: "21 de outubro de 2019"
output: pdf_document
---





```r
dt
```

```
## # A tibble: 960 x 7
##    base.algorithm strategy dataset       testset       params  perfs  print
##    <chr>          <chr>    <chr>         <chr>         <list>  <list> <chr>
##  1 CART           br       /home/lucasm… /home/lucasm… <tibbl… <name… Inst…
##  2 CART           br       /home/lucasm… /home/lucasm… <tibbl… <name… Inst…
##  3 CART           br       /home/lucasm… /home/lucasm… <tibbl… <name… Inst…
##  4 CART           br       /home/lucasm… /home/lucasm… <tibbl… <name… Inst…
##  5 CART           br       /home/lucasm… /home/lucasm… <tibbl… <name… Inst…
##  6 CART           brplus   /home/lucasm… /home/lucasm… <tibbl… <name… Inst…
##  7 CART           brplus   /home/lucasm… /home/lucasm… <tibbl… <name… Inst…
##  8 CART           brplus   /home/lucasm… /home/lucasm… <tibbl… <name… Inst…
##  9 CART           brplus   /home/lucasm… /home/lucasm… <tibbl… <name… Inst…
## 10 CART           brplus   /home/lucasm… /home/lucasm… <tibbl… <name… Inst…
## # … with 950 more rows
```



```r
ml_metrics <- dt %>%
  select(-params) %>%
  mutate(
    strategy = str_to_upper(strategy),
    perfs = map(perfs, ~enframe(.x$perf$multilabel))
  ) %>%
  unnest(cols = c(perfs))

plt <- ml_metrics %>%
  # filter(strategy == 'BR') %>%
  filter(name %in% c('average-precision', 'macro-AUC')) %>%
  mutate(
    algo = paste(strategy)
  ) %>%
  group_by(algo, strategy, print, base.algorithm, name) %>%
  summarise(ymin = min(value), ymax = max(value)) %>%
  ggplot(aes(x = algo, ymin = ymin, ymax = ymax, colour = print)) +
  geom_errorbar() +
  # geom_jitter(width = 0.2, size= 0.5) +
  labs(x = NULL, y = NULL) +
  facet_grid(rows = vars(base.algorithm), cols = vars(name), scales = 'free') +
  coord_flip() +
  theme_bw() +
  theme(legend.position = 'bottom', legend.title = element_blank())
plt
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1-1.png)

```r
# ggsave(
#   "rec-mh.pdf",
#   plt,
#   width = 7,
#   height = 10,
#   dpi = 300,
#   units = 'in'
# )
```


```r
kwCdBa <- function(ml_metrics, metric) {
  test_dt <- ml_metrics %>%
    filter(name %in% c(metric)) %>%
    mutate(sample = paste(str_extract(dataset, "dataset_.?"), print, name, strategy),
           value = if_else(is.na(value), 0, value)) %>%
    select(base.algorithm, sample, value) %>% 
    spread(base.algorithm, value) %>% 
    column_to_rownames("sample") %>%
    as.matrix() %>%
    scmamp::plotCD()
}
kwCd <- function(ml_metrics, metric) {
  test_dt <- ml_metrics %>%
    filter(name %in% c(metric)) %>%
    mutate(sample = paste(str_extract(dataset, "dataset_.?"), print, name, base.algorithm),
           value = if_else(is.na(value), 0, value))
  print(PMCMRplus::friedmanTest(
    test_dt$value,
    factor(test_dt$strategy),
    factor(test_dt$sample)
  ))
  ph_test <- PMCMRplus::frdAllPairsNemenyiTest(
    test_dt$value,
    factor(test_dt$strategy),
    factor(test_dt$sample)
  )
  dt <- test_dt %>% 
    select(strategy, sample, value) %>% 
    spread(strategy, value) %>% 
    column_to_rownames("sample") %>%
    as.matrix()
  print(ph_test)
  scmamp::plotCD(dt)
}

savePdf <- function(fplot, filename) {
  pdf("mh-rec-compare-macro-auc.pdf", width = 4, height = 2,
      family = 'Times', pointsize = 8)
  par(mar=c(.5,.5,.5,.5)-.5)
  fplot()
  dev.off()
}

kwCd(ml_metrics, 'average-precision')
```

```
## 
## 	Friedman rank sum test
## 
## data:  test_dt$value , factor(test_dt$strategy) and factor(test_dt$sample)
## Friedman chi-squared = 633.87, df = 15, p-value < 2.2e-16
## 
##         BR      BRPLUS  CC      CTRL    DBR     EBR     ECC     ESL    
## BRPLUS  1.00000 -       -       -       -       -       -       -      
## CC      0.99986 0.98109 -       -       -       -       -       -      
## CTRL    1.00000 1.00000 0.99977 -       -       -       -       -      
## DBR     0.98325 0.80720 1.00000 0.97872 -       -       -       -      
## EBR     0.97020 0.74693 1.00000 0.96321 1.00000 -       -       -      
## ECC     0.77195 0.37946 0.99931 0.74693 1.00000 1.00000 -       -      
## ESL     0.03585 0.00491 0.43428 0.03152 0.80720 0.85910 0.98857 -      
## HOMER   0.00011 6.1e-06 0.00836 8.7e-05 0.05212 0.07011 0.25809 0.99243
## LP      0.00331 0.00030 0.10369 0.00282 0.35323 0.42031 0.78401 1.00000
## MBR     0.00080 5.9e-05 0.03821 0.00067 0.17237 0.21711 0.55000 0.99989
## NS      0.00039 2.6e-05 0.02262 0.00033 0.11534 0.14895 0.43428 0.99931
## PRUDENT 0.00087 6.5e-05 0.04070 0.00073 0.18074 0.22692 0.56470 0.99991
## RAKEL   0.60861 0.23702 0.99435 0.57937 0.99998 1.00000 1.00000 0.99821
## RDBR    0.74693 0.35323 0.99897 0.72084 1.00000 1.00000 1.00000 0.99128
## RPC     1.00000 1.00000 0.99585 1.00000 0.90909 0.86839 0.53531 0.01122
##         HOMER   LP      MBR     NS      PRUDENT RAKEL   RDBR   
## BRPLUS  -       -       -       -       -       -       -      
## CC      -       -       -       -       -       -       -      
## CTRL    -       -       -       -       -       -       -      
## DBR     -       -       -       -       -       -       -      
## EBR     -       -       -       -       -       -       -      
## ECC     -       -       -       -       -       -       -      
## ESL     -       -       -       -       -       -       -      
## HOMER   -       -       -       -       -       -       -      
## LP      1.00000 -       -       -       -       -       -      
## MBR     1.00000 1.00000 -       -       -       -       -      
## NS      1.00000 1.00000 1.00000 -       -       -       -      
## PRUDENT 1.00000 1.00000 1.00000 1.00000 -       -       -      
## RAKEL   0.40651 0.90171 0.72084 0.60861 0.73401 -       -      
## RDBR    0.28028 0.80720 0.57937 0.46265 0.59402 1.00000 -      
## RPC     1.9e-05 0.00080 0.00017 7.9e-05 0.00019 0.36623 0.50604
```

![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-2-1.png)

```r
kwCd(ml_metrics, 'macro-AUC')
```

```
## 
## 	Friedman rank sum test
## 
## data:  test_dt$value , factor(test_dt$strategy) and factor(test_dt$sample)
## Friedman chi-squared = 572.35, df = 15, p-value < 2.2e-16
## 
##         BR    BRPLUS CC    CTRL  DBR   EBR   ECC   ESL   HOMER LP    MBR  
## BRPLUS  1.000 -      -     -     -     -     -     -     -     -     -    
## CC      1.000 1.000  -     -     -     -     -     -     -     -     -    
## CTRL    0.970 0.999  1.000 -     -     -     -     -     -     -     -    
## DBR     1.000 1.000  1.000 0.951 -     -     -     -     -     -     -    
## EBR     0.999 1.000  1.000 1.000 0.997 -     -     -     -     -     -    
## ECC     1.000 1.000  1.000 1.000 1.000 1.000 -     -     -     -     -    
## ESL     1.000 1.000  1.000 0.940 1.000 0.996 1.000 -     -     -     -    
## HOMER   0.839 0.550  0.340 0.032 0.886 0.122 0.434 0.902 -     -     -    
## LP      0.992 0.916  0.772 0.181 0.996 0.448 0.849 0.997 1.000 -     -    
## MBR     0.946 0.747  0.535 0.074 0.967 0.237 0.638 0.973 1.000 1.000 -    
## NS      1.000 1.000  1.000 1.000 1.000 1.000 1.000 1.000 0.340 0.772 0.535
## PRUDENT 1.000 1.000  1.000 0.999 1.000 1.000 1.000 1.000 0.535 0.909 0.734
## RAKEL   1.000 1.000  1.000 0.959 1.000 0.998 1.000 1.000 0.868 0.995 0.959
## RDBR    1.000 1.000  1.000 1.000 1.000 1.000 1.000 1.000 0.366 0.796 0.565
## RPC     0.902 0.990  0.999 1.000 0.859 1.000 0.997 0.839 0.013 0.093 0.034
##         NS    PRUDENT RAKEL RDBR 
## BRPLUS  -     -       -     -    
## CC      -     -       -     -    
## CTRL    -     -       -     -    
## DBR     -     -       -     -    
## EBR     -     -       -     -    
## ECC     -     -       -     -    
## ESL     -     -       -     -    
## HOMER   -     -       -     -    
## LP      -     -       -     -    
## MBR     -     -       -     -    
## NS      -     -       -     -    
## PRUDENT 1.000 -       -     -    
## RAKEL   1.000 1.000   -     -    
## RDBR    1.000 1.000   1.000 -    
## RPC     0.999 0.991   0.877 0.999
```

![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-2-2.png)

```r
kwCdBa(ml_metrics, 'average-precision')
```

![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-2-3.png)

```r
kwCdBa(ml_metrics, 'macro-AUC')
```

![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-2-4.png)

```r
kwCd(ml_metrics %>% filter(print == 'Groups'), 'average-precision')
```

```
## 
## 	Friedman rank sum test
## 
## data:  test_dt$value , factor(test_dt$strategy) and factor(test_dt$sample)
## Friedman chi-squared = 150.45, df = 15, p-value < 2.2e-16
## 
##         BR   BRPLUS CC   CTRL DBR  EBR  ECC  ESL  HOMER LP   MBR  NS  
## BRPLUS  1.00 -      -    -    -    -    -    -    -     -    -    -   
## CC      1.00 1.00   -    -    -    -    -    -    -     -    -    -   
## CTRL    1.00 1.00   1.00 -    -    -    -    -    -     -    -    -   
## DBR     1.00 1.00   1.00 1.00 -    -    -    -    -     -    -    -   
## EBR     1.00 1.00   1.00 1.00 1.00 -    -    -    -     -    -    -   
## ECC     1.00 1.00   1.00 1.00 1.00 1.00 -    -    -     -    -    -   
## ESL     0.99 0.94   1.00 0.97 1.00 1.00 1.00 -    -     -    -    -   
## HOMER   0.96 0.85   0.98 0.90 1.00 0.98 1.00 1.00 -     -    -    -   
## LP      0.87 0.68   0.93 0.76 0.99 0.93 0.99 1.00 1.00  -    -    -   
## MBR     0.71 0.48   0.81 0.56 0.96 0.81 0.96 1.00 1.00  1.00 -    -   
## NS      0.59 0.37   0.71 0.45 0.92 0.71 0.92 1.00 1.00  1.00 1.00 -   
## PRUDENT 0.73 0.51   0.83 0.59 0.97 0.83 0.97 1.00 1.00  1.00 1.00 1.00
## RAKEL   1.00 1.00   1.00 1.00 1.00 1.00 1.00 1.00 1.00  1.00 1.00 0.99
## RDBR    1.00 1.00   1.00 1.00 1.00 1.00 1.00 1.00 1.00  1.00 0.99 0.97
## RPC     1.00 1.00   1.00 1.00 1.00 1.00 1.00 0.92 0.81  0.62 0.42 0.32
##         PRUDENT RAKEL RDBR
## BRPLUS  -       -     -   
## CC      -       -     -   
## CTRL    -       -     -   
## DBR     -       -     -   
## EBR     -       -     -   
## ECC     -       -     -   
## ESL     -       -     -   
## HOMER   -       -     -   
## LP      -       -     -   
## MBR     -       -     -   
## NS      -       -     -   
## PRUDENT -       -     -   
## RAKEL   1.00    -     -   
## RDBR    0.99    1.00  -   
## RPC     0.45    1.00  1.00
```

![plot of chunk unnamed-chunk-2](figure/unnamed-chunk-2-5.png)



```r
ranks <- ml_metrics %>%
  filter(name %in% c('macro-AUC')) %>%
  group_by(strategy) %>%
  summarise(mean_rank = mean(rank(value)))

label_metrics <- dt %>%
  select(-params) %>%
  mutate(
    perfs = map(perfs, ~as_tibble(.x$perf$labels) %>%
                  mutate(mh = rownames(.x$perf$labels)))
  ) %>%
  unnest()

# plt1 <- label_metrics %>%
#   filter(strategy == 'brplus') %>%
#   mutate(
#     mh = factor(str_to_upper(str_remove(mh, 'rec_')),
#                 levels = c('IHC', 'ISA', 'TS', 'ACO', 'ILS', 'IG'),
#                 labels = c('HC', 'SA', 'TS', 'ACO', 'ILS', 'IG')),
#     algo = paste(base.algorithm, mh)
#   ) %>%
#   ggplot() +
#   facet_wrap(~mh, scales = 'free_x') +
#   geom_boxplot(aes(x = base.algorithm, y = AUC, fill = feature_selection)) +
#   labs(x = NULL, y = NULL) +
#   theme_bw() +
#   theme(legend.position = 'bottom', legend.title = element_blank())
# plt1
# ggsave(
#   "mh-rec-individual.pdf",
#   plt1,
#   width = 7,
#   height = 4,
#   dpi = 300,
#   units = 'in'
# )
```


```r
dt <- read_csv("data/models_0911/multilabel_mh_recommendation_feature_selection/dataset_1.csv")
```

```
## Error: 'data/models_0911/multilabel_mh_recommendation_feature_selection/dataset_1.csv' does not exist in current working directory ('/home/lucasmp/projects/git/evolutionary_tunners/R/testing').
```

```r
train_dt <- dt %>%
  rename(output = rec_ts) %>%
  mutate(output = factor(output)) %>%
  select(-starts_with('rec_'), -folds)
```

```
## Error in .f(.x[[i]], ...): objeto 'rec_ts' não encontrado
```

```r
md <- caret::train(
  output ~ .,
  data = train_dt,
  'rpart',
  tuneGrid = expand.grid(
    cp = c(0.02)
  )
)
```

```
## Error in eval(expr, p): objeto 'train_dt' não encontrado
```

```r
# pdf("cart-ts-rec.pdf", width = 3, height = 2,
#     family = 'Times', pointsize = 8)
# par(mar=c(.5,.5,.5,.5)-.5)
rpart.plot(md$finalModel, type = 3, fallen.leaves = T, extra = 100)
```

```
## Error in rpart.plot(md$finalModel, type = 3, fallen.leaves = T, extra = 100): objeto 'md' não encontrado
```

```r
# dev.off()
```



```r
library(caret)
load(here::here("data/models_0911/multilabel_mh_recommendation/dataset_1.csv_models/br/RF/mtry=13_best.Rdata"))

rf_imps <- map2_dfr(model$models, model$labels, 
                    ~ as.data.frame(varImp(.x)) %>% 
                      rownames_to_column() %>%
                      mutate(mh = .y))
plt <- rf_imps %>%
  mutate(mh = str_to_upper(str_extract(mh, "[a-z]*$"))) %>%
  group_by(mh, rowname) %>%
  mutate(total = sum(Overall)) %>%
  mutate(order = rank(total)) %>%
  transform(rowname = reorder(rowname, total)) %>%
  ggplot() +
  geom_col(aes(fill = mh, x = rowname, y = Overall)) +
  coord_flip() +
  labs(x = NULL, y = "importance", fill = "MH") +
  theme_bw()
plt
```

![plot of chunk feature-importance-example](figure/feature-importance-example-1.png)

```r
# ggsave(
#   "feature-importance.pdf",
#   plt,
#   width = 150,
#   height = 150,
#   dpi = 300,
#   units = 'mm'
# ) 
```
