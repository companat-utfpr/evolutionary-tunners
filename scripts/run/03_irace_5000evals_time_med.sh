#!/bin/sh
Rscript R/run_irace.R --budget med   --dist taill-like --corr jcorr --stopping_criterium TIME >> logs/0`date`.err & \
Rscript R/run_irace.R --budget med   --dist binom      --corr jcorr --stopping_criterium TIME >> logs/1`date`.err & \
Rscript R/run_irace.R --budget med   --dist exp        --corr jcorr --stopping_criterium TIME >> logs/2`date`.err & \
Rscript R/run_irace.R --budget med   --dist taill-like --corr mcorr --stopping_criterium TIME >> logs/3`date`.err & \
Rscript R/run_irace.R --budget med   --dist binom      --corr mcorr --stopping_criterium TIME >> logs/4`date`.err 
Rscript R/run_irace.R --budget med   --dist exp        --corr mcorr --stopping_criterium TIME >> logs/5`date`.err & \
Rscript R/run_irace.R --budget med   --dist taill-like --corr rand  --stopping_criterium TIME >> logs/6`date`.err & \
Rscript R/run_irace.R --budget med   --dist binom      --corr rand  --stopping_criterium TIME >> logs/7`date`.err & \
Rscript R/run_irace.R --budget med   --dist exp        --corr rand  --stopping_criterium TIME >> logs/8`date`.err 
Rscript R/run_irace.R --budget high  --dist taill-like --corr jcorr --stopping_criterium TIME >> logs/9`date`.err & \
Rscript R/run_irace.R --budget high  --dist binom      --corr jcorr --stopping_criterium TIME >> logs/a`date`.err & \
Rscript R/run_irace.R --budget high  --dist exp        --corr jcorr --stopping_criterium TIME >> logs/b`date`.err & \
Rscript R/run_irace.R --budget high  --dist taill-like --corr mcorr --stopping_criterium TIME >> logs/c`date`.err & \
Rscript R/run_irace.R --budget high  --dist binom      --corr mcorr --stopping_criterium TIME >> logs/d`date`.err 
Rscript R/run_irace.R --budget high  --dist exp        --corr mcorr --stopping_criterium TIME >> logs/e`date`.err & \
Rscript R/run_irace.R --budget high  --dist taill-like --corr rand  --stopping_criterium TIME >> logs/f`date`.err & \
Rscript R/run_irace.R --budget high  --dist binom      --corr rand  --stopping_criterium TIME >> logs/g`date`.err & \
Rscript R/run_irace.R --budget high  --dist exp        --corr rand  --stopping_criterium TIME >> logs/h`date`.err
