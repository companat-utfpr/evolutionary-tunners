#!/bin/sh
#Rscript R/run_irace.R --budget low  --dist taill-like --stopping_criterium TIME >> logs/0`date`.err
#Rscript R/run_irace.R --budget low  --dist binom --corr jcorr --stopping_criterium TIME >> logs/1`date`.err
Rscript R/run_irace.R --budget low  --dist binom --corr mcorr --stopping_criterium TIME >> logs/0`date`.err & \
Rscript R/run_irace.R --budget low  --dist binom --corr rand  --stopping_criterium TIME >> logs/1`date`.err & \
Rscript R/run_irace.R --budget low  --dist exp   --corr jcorr --stopping_criterium TIME >> logs/2`date`.err & \
Rscript R/run_irace.R --budget low  --dist exp   --corr mcorr --stopping_criterium TIME >> logs/3`date`.err & \
Rscript R/run_irace.R --budget low  --dist exp   --corr rand  --stopping_criterium TIME >> logs/4`date`.err

