#!/bin/sh

# Rscript R/run_irace.R --budget low --dist taill-like  --corr rand  --stopping_criterium EVALS --mh TS >> logs/1`date`.err & \
# Rscript R/run_irace.R --budget low --dist binom       --corr rand  --stopping_criterium EVALS --mh TS >> logs/2`date`.err & \
# Rscript R/run_irace.R --budget low --dist exp         --corr rand  --stopping_criterium EVALS --mh TS >> logs/3`date`.err & \
# Rscript R/run_irace.R --budget low --dist taill-like  --corr mcorr --stopping_criterium EVALS --mh TS >> logs/4`date`.err & \
# Rscript R/run_irace.R --budget low --dist binom       --corr mcorr --stopping_criterium EVALS --mh TS >> logs/5`date`.err & \
# Rscript R/run_irace.R --budget low --dist exp         --corr mcorr --stopping_criterium EVALS --mh TS >> logs/6`date`.err & \
# Rscript R/run_irace.R --budget low --dist taill-like  --corr jcorr --stopping_criterium EVALS --mh TS >> logs/7`date`.err & \
# Rscript R/run_irace.R --budget low --dist binom       --corr jcorr --stopping_criterium EVALS --mh TS >> logs/8`date`.err & \
# Rscript R/run_irace.R --budget low --dist exp         --corr jcorr --stopping_criterium EVALS --mh TS >> logs/9`date`.err
# Rscript R/run_irace.R --budget med --dist taill-like  --corr rand  --stopping_criterium EVALS --mh TS >> logs/1`date`.err & \
# Rscript R/run_irace.R --budget med --dist binom       --corr rand  --stopping_criterium EVALS --mh TS >> logs/2`date`.err & \
# Rscript R/run_irace.R --budget med --dist exp         --corr rand  --stopping_criterium EVALS --mh TS >> logs/3`date`.err & \
# Rscript R/run_irace.R --budget med --dist taill-like  --corr mcorr --stopping_criterium EVALS --mh TS >> logs/4`date`.err & \
# Rscript R/run_irace.R --budget med --dist binom       --corr mcorr --stopping_criterium EVALS --mh TS >> logs/5`date`.err & \
# Rscript R/run_irace.R --budget med --dist exp         --corr mcorr --stopping_criterium EVALS --mh TS >> logs/6`date`.err & \
# Rscript R/run_irace.R --budget med --dist taill-like  --corr jcorr --stopping_criterium EVALS --mh TS >> logs/7`date`.err & \
# Rscript R/run_irace.R --budget med --dist binom       --corr jcorr --stopping_criterium EVALS --mh TS >> logs/8`date`.err & \
# Rscript R/run_irace.R --budget med --dist exp         --corr jcorr --stopping_criterium EVALS --mh TS >> logs/9`date`.err
# Rscript R/run_irace.R --budget high --dist taill-like --corr rand  --stopping_criterium EVALS --mh TS >> logs/1`date`.err & \
# Rscript R/run_irace.R --budget high --dist binom      --corr rand  --stopping_criterium EVALS --mh TS >> logs/2`date`.err & \
# Rscript R/run_irace.R --budget high --dist exp        --corr rand  --stopping_criterium EVALS --mh TS >> logs/3`date`.err & \
# Rscript R/run_irace.R --budget high --dist taill-like --corr mcorr --stopping_criterium EVALS --mh TS >> logs/4`date`.err & \
# Rscript R/run_irace.R --budget high --dist binom      --corr mcorr --stopping_criterium EVALS --mh TS >> logs/5`date`.err & \
# Rscript R/run_irace.R --budget high --dist exp        --corr mcorr --stopping_criterium EVALS --mh TS >> logs/6`date`.err & \
# Rscript R/run_irace.R --budget high --dist taill-like --corr jcorr --stopping_criterium EVALS --mh TS >> logs/7`date`.err & \
# Rscript R/run_irace.R --budget high --dist binom      --corr jcorr --stopping_criterium EVALS --mh TS >> logs/8`date`.err & \
# Rscript R/run_irace.R --budget high --dist exp        --corr jcorr --stopping_criterium EVALS --mh TS >> logs/9`date`.err

Rscript R/run_irace.R --budget high --stopping_criterium EVALS --mh TS >> logs/1`date`.err
