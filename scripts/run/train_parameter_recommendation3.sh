rscript=Rscript
script=R/group_experiments/train_tree_models_parameters_recommendation.R
# $rscript $script prudent & 
# $rscript $script cc      &
# $rscript $script brplus	 
# $rscript $script ctrl    &
# $rscript $script dbr     &
# $rscript $script ebr	   
# $rscript $script ecc     &
# $rscript $script eps     &
# $rscript $script esl     
# $rscript $script lift    &
# $rscript $script mbr     &
# $rscript $script ns      
# $rscript $script rakel   &
# $rscript $script rdbr    &
              
#folder='parameters_recommendation'
for folder in parameters_recommendation parameters_recommendation_feature_selection
  do
$rscript $script "$folder" ISA ISA.Beta                          
$rscript $script "$folder" TS TS.Algo                           
$rscript $script "$folder" TS TS.Comp.Strat                     
$rscript $script "$folder" TS TS.Init.Strat                     
$rscript $script "$folder" TS TS.Neighborhood.Size              
$rscript $script "$folder" TS TS.Neighborhood.Strat             
done
#
