Rscript R/FLA/local_optima_networks.R --budget med --no_jobs 100 --dist binom,exp --inst_n 1,1 --mh all --stopping_criterium EVALS & \
Rscript R/FLA/local_optima_networks.R --budget med --no_jobs 100 --dist binom,exp --inst_n 5,5 --mh all --stopping_criterium EVALS & \
Rscript R/FLA/local_optima_networks.R --budget med --no_jobs 100 --dist binom,exp --inst_n 2,2 --mh all --stopping_criterium EVALS & \
Rscript R/FLA/local_optima_networks.R --budget med --no_jobs 100 --dist binom,exp --inst_n 3,3 --mh all --stopping_criterium EVALS & \
Rscript R/FLA/local_optima_networks.R --budget med --no_jobs 100 --dist binom,exp --inst_n 4,4 --mh all --stopping_criterium EVALS & wait
