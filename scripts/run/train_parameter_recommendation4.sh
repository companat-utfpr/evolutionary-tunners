rscript=Rscript
script=R/group_experiments/train_tree_models_parameters_recommendation.R
# $rscript $script prudent & 
# $rscript $script cc      &
# $rscript $script brplus	 
# $rscript $script ctrl    &
# $rscript $script dbr     &
# $rscript $script ebr	   
# $rscript $script ecc     &
# $rscript $script eps     &
# $rscript $script esl     
# $rscript $script lift    &
# $rscript $script mbr     &
# $rscript $script ns      
# $rscript $script rakel   &
# $rscript $script rdbr    &
              
#folder='parameters_recommendation'
for folder in parameters_recommendation parameters_recommendation_feature_selection
  do
$rscript $script "$folder" TS TS.Aspiration                     
$rscript $script "$folder" TS TS.Tabu.List.Type                 
$rscript $script "$folder" TS TS.How.Long.Taboo                 
$rscript $script "$folder" TS TS.Max.Stagnation.Window          
$rscript $script "$folder" TS TS.How.Long.Rnd.Taboo             
$rscript $script "$folder" TS TS.Max.Size.TL                    
done
#
