#!/bin/sh

Rscript R/FLA/local_optima_networks_fast.R --budget med --stopping_criterium EVALS --mh all --type PERM,NOWAIT,NOIDLE --no_jobs 100 --dist taill-like --corr rand & \
Rscript R/FLA/local_optima_networks_fast.R --budget med --stopping_criterium EVALS --mh all --type PERM,NOWAIT,NOIDLE --no_jobs 100 --dist taill-like --corr jcorr & \
Rscript R/FLA/local_optima_networks_fast.R --budget med --stopping_criterium EVALS --mh all --type PERM,NOWAIT,NOIDLE --no_jobs 100 --dist taill-like --corr mcorr & \
wait

Rscript R/FLA/local_optima_networks_fast.R --budget med --stopping_criterium EVALS --mh all --type PERM,NOWAIT,NOIDLE --no_jobs 10,20,30 --dist binom --corr rand & \
Rscript R/FLA/local_optima_networks_fast.R --budget med --stopping_criterium EVALS --mh all --type PERM,NOWAIT,NOIDLE --no_jobs 10,20,30 --dist binom --corr jcorr & \
Rscript R/FLA/local_optima_networks_fast.R --budget med --stopping_criterium EVALS --mh all --type PERM,NOWAIT,NOIDLE --no_jobs 10,20,30 --dist binom --corr mcorr & \
wait

Rscript R/FLA/local_optima_networks_fast.R --budget med --stopping_criterium EVALS --mh all --type PERM,NOWAIT,NOIDLE --no_jobs 10,20,30 --dist exp --corr rand & \
Rscript R/FLA/local_optima_networks_fast.R --budget med --stopping_criterium EVALS --mh all --type PERM,NOWAIT,NOIDLE --no_jobs 10,20,30 --dist exp --corr jcorr & \
Rscript R/FLA/local_optima_networks_fast.R --budget med --stopping_criterium EVALS --mh all --type PERM,NOWAIT,NOIDLE --no_jobs 10,20,30 --dist exp --corr mcorr & \
wait

