#!/bin/sh
logsfdr=logs/old_`date`
mkdir $logsfdr
mv logs/* logs/$logsfdr

Rscript R/run_irace.R --objective FLOWTIME --dist binom      --corr rand  --no_jobs 30 --no_machines 5  --stopping_criterium FITNESS >> logs/0`date`.err & \
Rscript R/run_irace.R --objective MAKESPAN --dist exp        --corr rand  --no_jobs 30 --no_machines 10 --stopping_criterium FITNESS >> logs/1`date`.err & \
Rscript R/run_irace.R --objective FLOWTIME --dist taill-like --corr rand  --no_jobs 30 --no_machines 5  --stopping_criterium FITNESS >> logs/2`date`.err & \
Rscript R/run_irace.R --objective FLOWTIME --dist exp        --corr mcorr --no_jobs 30 --no_machines 5  --stopping_criterium FITNESS >> logs/3`date`.err & \
Rscript R/run_irace.R --objective FLOWTIME --dist binom      --corr rand  --no_jobs 30 --no_machines 20 --stopping_criterium FITNESS >> logs/4`date`.err & \
Rscript R/run_irace.R --objective FLOWTIME --dist binom      --corr rand  --no_jobs 30 --no_machines 10 --stopping_criterium FITNESS >> logs/5`date`.err

