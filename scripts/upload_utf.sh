#!/bin/bash

machine=$1
folder=$2
scp $folder utf:~
ssh utf "scp $folder ${machine}:~/git/evolutionary-tunners/ && rm $folder"

