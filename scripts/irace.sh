#!/bin/sh

Rscript R/run_irace.R --budget low --dist binom   --corr rand  --objective MAKESPAN --stopping_criterium FITNESS --no_jobs 50 >> logs/1`date`.err & \
Rscript R/run_irace.R --budget low --dist binom   --corr mcorr --objective MAKESPAN --stopping_criterium FITNESS --no_jobs 50 >> logs/2`date`.err & \
Rscript R/run_irace.R --budget low --dist binom   --corr jcorr --objective MAKESPAN --stopping_criterium FITNESS --no_jobs 50 >> logs/3`date`.err & \
Rscript R/run_irace.R --budget low --dist binom   --corr rand  --objective FLOWTIME --stopping_criterium FITNESS --no_jobs 50 >> logs/4`date`.err & \
Rscript R/run_irace.R --budget low --dist binom   --corr mcorr --objective FLOWTIME --stopping_criterium FITNESS --no_jobs 50 >> logs/5`date`.err & \
Rscript R/run_irace.R --budget low --dist binom   --corr jcorr --objective FLOWTIME --stopping_criterium FITNESS --no_jobs 50 >> logs/6`date`.err & \
wait

Rscript R/run_irace.R --budget med --dist binom   --corr rand  --objective MAKESPAN --stopping_criterium FITNESS --no_jobs 50 >> logs/1`date`.err & \
Rscript R/run_irace.R --budget med --dist binom   --corr mcorr --objective MAKESPAN --stopping_criterium FITNESS --no_jobs 50 >> logs/2`date`.err & \
Rscript R/run_irace.R --budget med --dist binom   --corr jcorr --objective MAKESPAN --stopping_criterium FITNESS --no_jobs 50 >> logs/3`date`.err & \
Rscript R/run_irace.R --budget med --dist binom   --corr rand  --objective FLOWTIME --stopping_criterium FITNESS --no_jobs 50 >> logs/4`date`.err & \
Rscript R/run_irace.R --budget med --dist binom   --corr mcorr --objective FLOWTIME --stopping_criterium FITNESS --no_jobs 50 >> logs/5`date`.err & \
Rscript R/run_irace.R --budget med --dist binom   --corr jcorr --objective FLOWTIME --stopping_criterium FITNESS --no_jobs 50 >> logs/6`date`.err & \
wait

Rscript R/run_irace.R --budget high --dist binom   --corr rand  --objective MAKESPAN --stopping_criterium FITNESS --no_jobs 50 >> logs/1`date`.err & \
Rscript R/run_irace.R --budget high --dist binom   --corr mcorr --objective MAKESPAN --stopping_criterium FITNESS --no_jobs 50 >> logs/2`date`.err & \
Rscript R/run_irace.R --budget high --dist binom   --corr jcorr --objective MAKESPAN --stopping_criterium FITNESS --no_jobs 50 >> logs/3`date`.err & \
Rscript R/run_irace.R --budget high --dist binom   --corr rand  --objective FLOWTIME --stopping_criterium FITNESS --no_jobs 50 >> logs/4`date`.err & \
Rscript R/run_irace.R --budget high --dist binom   --corr mcorr --objective FLOWTIME --stopping_criterium FITNESS --no_jobs 50 >> logs/5`date`.err & \
Rscript R/run_irace.R --budget high --dist binom   --corr jcorr --objective FLOWTIME --stopping_criterium FITNESS --no_jobs 50 >> logs/6`date`.err & \
wait

