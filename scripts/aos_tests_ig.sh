exec="/home/lucasmp/dev/evolutionary_tunners/build/main/fsp_solver"
data="/home/lucasmp/dev/evolutionary_tunners/data"
folder=ig
seed=123
objective=MAKESPAN
algo=2
aos=0
ds=4
for instance in $(cat /home/lucasmp/dev/evolutionary_tunners/scripts/instances.txt)
do
  for aos in 4
  do
    echo $instance $ds
    taskset -c 1 $exec \
      --seed=$seed \
      --data_folder=$data \
      --mh=IG \
      --problem_names=problem,type,objective,budget,instance,stopping_criterium \
      --problem_values=FSP,PERM,$objective,med,$instance,FIXEDTIME \
      --params_names=IG.Init.Strat,IG.Comp.Strat,IG.Neighborhood.Size,IG.Neighborhood.Strat,IG.Local.Search,IG.Accept,IG.Accept.Temperature,IG.Algo,IG.Destruction.Size,IG.LS.Single.Step,IG.LSPS.Local.Search,IG.LSPS.Single.Step,IG.AOS.Strategy \
      --params_values=1,0,9.9999,0,3,2,0.5,$algo,$ds,0,0,0,$aos > pm_aos=${aos}_${instance}.out
  done
done