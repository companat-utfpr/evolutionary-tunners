#!/bin/bash

machine=$1
folder=$2
ssh utf "ssh ${machine} \"tar cvzf download-${machine}.tar.gz ${folder}\" && scp ${machine}:download-${machine}.tar.gz ~/download-${machine}.tar.gz"
scp utf:download-${machine}.tar.gz download-${machine}.tar.gz
ssh utf "ssh ${machine} \"rm download-${machine}.tar.gz\" && rm download-${machine}.tar.gz"
mkdir download-${machine}
tar xzvf download-${machine}.tar.gz -C download-${machine}

