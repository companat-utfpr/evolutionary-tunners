#/bin/sh
rscript=Rscript
script=R/group_experiments/train_tree_models_parameters_recommendation.R
# $rscript $script prudent & 
# $rscript $script cc      &
# $rscript $script brplus	 
# $rscript $script ctrl    &
# $rscript $script dbr     &
# $rscript $script ebr	   
# $rscript $script ecc     &
# $rscript $script eps     &
# $rscript $script esl     
# $rscript $script lift    &
# $rscript $script mbr     &
# $rscript $script ns      
# $rscript $script rakel   &
# $rscript $script rdbr    &
              
#folder='parameters_recommendation'
#parameters_recommendation_feature_selection
for folder in parameters_recommendation 
do             
  $rscript $script "$folder" ACO ACO.Comp.Strat                    
  $rscript $script "$folder" ACO ACO.Init.Strat                    
  $rscript $script "$folder" ACO ACO.Neighborhood.Size             
  $rscript $script "$folder" ACO ACO.Neighborhood.Strat            
  $rscript $script "$folder" ACO ACO.Local.Search                  
  $rscript $script "$folder" ACO ACO.LS.Single.Step                
  $rscript $script "$folder" ACO ACO.T.Min.Factor                  
  $rscript $script "$folder" ACO ACO.Rho                           
  $rscript $script "$folder" ACO ACO.P0                            
  $rscript $script "$folder" IG IG.Comp.Strat                      
  $rscript $script "$folder" IG IG.Init.Strat                      
  $rscript $script "$folder" IG IG.Neighborhood.Size               
  $rscript $script "$folder" IG IG.Neighborhood.Strat              
  $rscript $script "$folder" IG IG.Local.Search                    
  $rscript $script "$folder" IG IG.Accept                          
  $rscript $script "$folder" IG IG.Algo                            
  $rscript $script "$folder" IG IG.Destruction.Size                
  $rscript $script "$folder" IG IG.LS.Single.Step                  
  $rscript $script "$folder" IG IG.Accept.Temperature              
  $rscript $script "$folder" IG IG.LSPS.Local.Search               
  $rscript $script "$folder" IG IG.LSPS.Single.Step                
  $rscript $script "$folder" IHC IHC.Algo                          
  $rscript $script "$folder" IHC IHC.Comp.Strat                    
  $rscript $script "$folder" IHC IHC.Init.Strat                    
  $rscript $script "$folder" IHC IHC.Neighborhood.Strat            
  $rscript $script "$folder" IHC IHC.Neighborhood.Size             
  $rscript $script "$folder" ILS ILS.Algo                          
  $rscript $script "$folder" ILS ILS.Comp.Strat                    
  $rscript $script "$folder" ILS ILS.Init.Strat                    
  $rscript $script "$folder" ILS ILS.Neighborhood.Size             
  $rscript $script "$folder" ILS ILS.Neighborhood.Strat            
  $rscript $script "$folder" ILS ILS.Accept                        
  $rscript $script "$folder" ILS ILS.LS.Single.Step                
  $rscript $script "$folder" ILS ILS.Perturb                       
  $rscript $script "$folder" ILS ILS.Accept.Temperature            
  $rscript $script "$folder" ILS ILS.Perturb.Restart.Init          
  $rscript $script "$folder" ILS ILS.Perturb.Restart.Threshold     
  $rscript $script "$folder" ILS ILS.Perturb.No.Kick               
  $rscript $script "$folder" ILS ILS.Perturb.Kick.Strength         
  $rscript $script "$folder" ILS ILS.Perturb.NILS.Escape           
  $rscript $script "$folder" ILS ILS.Perturb.NILS.MNS              
  $rscript $script "$folder" ILS ILS.Perturb.NILS.Destruction.Size 
  $rscript $script "$folder" ILS ILS.Perturb.NILS.No.Kick          
  $rscript $script "$folder" ISA ISA.Algo                          
  $rscript $script "$folder" ISA ISA.Comp.Strat                    
  $rscript $script "$folder" ISA ISA.Init.Strat                    
  $rscript $script "$folder" ISA ISA.Neighborhood.Size             
  $rscript $script "$folder" ISA ISA.Span.Simple                   
  $rscript $script "$folder" ISA ISA.Span.Tries.Max                
  $rscript $script "$folder" ISA ISA.Span.Move.Max                 
  $rscript $script "$folder" ISA ISA.Nb.Span.Max                   
  $rscript $script "$folder" ISA ISA.Init.Temp                     
  $rscript $script "$folder" ISA ISA.Final.Temp                    
  $rscript $script "$folder" ISA ISA.Alpha                         
  $rscript $script "$folder" ISA ISA.T                             
  $rscript $script "$folder" ISA ISA.Beta                          
  $rscript $script "$folder" TS TS.Algo                            
  $rscript $script "$folder" TS TS.Comp.Strat                      
  $rscript $script "$folder" TS TS.Init.Strat                      
  $rscript $script "$folder" TS TS.Neighborhood.Size               
  $rscript $script "$folder" TS TS.Neighborhood.Strat              
  $rscript $script "$folder" TS TS.Aspiration                      
  $rscript $script "$folder" TS TS.Tabu.List.Type                  
  $rscript $script "$folder" TS TS.How.Long.Taboo                  
  $rscript $script "$folder" TS TS.Max.Stagnation.Window           
  $rscript $script "$folder" TS TS.How.Long.Rnd.Taboo              
  $rscript $script "$folder" TS TS.Max.Size.TL                     
done
