#!/bin/python3

import subprocess as sp

folder = "../../data/instances/generated_intances/generated_instances_all/"
results = "results/"

for dist in ['taill-like', 'binom', 'exp', 'mcorr', 'jcorr']:
    for no_jobs in [10, 20, 30]:
        for no_machines in [5]:
            for i in range(101, 151):
                instance_name = "{:s}_{:d}_{:d}_{:02d}{:02d}{:03d}.gen".format(dist, no_jobs, no_machines, no_jobs, no_machines, i)
                print(instance_name)
                with open(results + instance_name, "w") as outfile:
                    sp.call(["./sched_pflowshop", folder + instance_name], stdout=outfile)

