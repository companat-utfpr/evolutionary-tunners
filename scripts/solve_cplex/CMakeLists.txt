cmake_minimum_required(VERSION 3.0.0)
project(pflowshop_cplex VERSION 0.1.0)

include(CTest)
enable_testing()

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
#set(CPLEX_DIR /opt/ibm/ILOG/CPLEX_Studio128)
find_package(CPLEX)

add_executable(sched_pflowshop sched_pflowshop.cpp)
add_definitions(-DIL_STD -O -DNDEBUG -fPIC -fstrict-aliasing -pedantic -Wall -fexceptions -frounding-math -Wno-long-long -m64 -DILOUSEMT -D_REENTRANT -DILM_REENTRANT)
target_include_directories(sched_pflowshop PUBLIC ${CPLEX_INCLUDE_DIR} ${CPLEX_CONCERT_INCLUDE_DIR} ${CPLEX_CP_INCLUDE_DIR})
target_link_libraries(sched_pflowshop ${CPLEX_LIBRARY} ${CPLEX_CONCERT_LIBRARY} ${CPLEX_CP_LIBRARY})

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
