FILENAME=`pwd`/deploy/et_$(date +%F_%H-%M-%S).tar
echo "Creating file $FILENAME ..."
git archive HEAD > $FILENAME
echo "Copying file $FILENAME to remote..."
scp $FILENAME utf:~/git/
echo "Ssh to remote..."
ssh utf

