mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
if [ $? -eq 0 ]; then
    cd ../R
    /usr/bin/R R CMD INSTALL --no-multiarch --with-keep.source --preclean RFlowshopSolvers
fi
